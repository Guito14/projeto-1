﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneLevel3 : MonoBehaviour
{
    public GameObject thePlayer;
    public GameObject cutSceneCam;

    QuestNivel3 quest3;

    void Start()
    {
        this.gameObject.GetComponent<CutsceneLevel3>().enabled = false;
        quest3 = GameObject.Find("MongeNivel3").GetComponent<QuestNivel3>();
    }

    void OnTriggerEnter(Collider other)
    {
        this.gameObject.GetComponent<CutsceneLevel3>().enabled = true;
        if (other.gameObject.tag == "Player" && quest3.isQuestCompleted)
        {
            cutSceneCam.SetActive(true);
            thePlayer.SetActive(false);
            StartCoroutine(FinishCut());

        }

    }


    IEnumerator FinishCut()
    {
        yield return new WaitForSeconds(10);
        thePlayer.SetActive(true);
        cutSceneCam.SetActive(false);
    }
}
