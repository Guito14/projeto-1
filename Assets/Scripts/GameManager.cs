﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static GameManager instanciar;
    //private questMaca Quest1;
    private QuestNivel1 Quest1;
    public int DogsKilled;
    QuestNivel3 quest3;
    Quest_Nivel_4 quest4;
    public int macasTotal;
    public int TigersKilled;
    public int fishcount;
    //QuestNivel2 quest2;
    //public int nozesTotal;



    private void Awake()
    {

        DontDestroyOnLoad(gameObject);
        instanciar = this;
        Quest1 = GameObject.Find("Monge_Lvl_1").GetComponent<QuestNivel1>();
        quest3 = GameObject.Find("MongeNivel3").GetComponent<QuestNivel3>();
        quest4 = GameObject.Find("MongeNivel4").GetComponent<Quest_Nivel_4>();
        //quest2 = GameObject.Find("Monge_level_2").GetComponent<QuestNivel2>();
       

    }

    public float timer;

    //  Update is called once per frame
    void Update()
    {


        if (SceneManager.GetActiveScene().name == "HumanEvolution")
        {
            timer = timer + Time.deltaTime;

        }

    }

    public void DogsKilledCounter()
    {
        if (quest3.isQuestStart)
            DogsKilled++;

    }
    public void fishCounter()
    {
        if (quest4.isQuestStart)
            fishcount++;

    }
    public void tigersKilledCount()
    {
        if (quest4.isQuestStart)
            TigersKilled++;
    }
    public void MacasTotalCounter()
    {
        if (Quest1.isQuestStart)
            macasTotal++;
    }
    //public void NozesTotalCounter()
    //{
    //    if (quest2.isQuestStart)
    //        nozesTotal++;
    //}


}

