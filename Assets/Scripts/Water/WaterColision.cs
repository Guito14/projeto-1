﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterColision : MonoBehaviour
{

    private VitalsManager vm;

    public bool colWater = false;
    // Start is called before the first frame update
    void Start()
    {
        vm = GameObject.Find("FirstPersonPlayer").GetComponent<VitalsManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (colWater)
            //vm.currentHealth--;
            vm.GetComponent<VitalsManager>().currentHealth--;
        vm.healthBar.SetHealth(vm.currentHealth);
    }

    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            colWater = true;

        }
    }

    void OnTriggerExit(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            colWater = false;
        }
    }
}
