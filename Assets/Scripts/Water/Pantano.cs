﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pantano : MonoBehaviour
{

    public bool slow = false;
    PlayerMovementScript pm;

    // Start is called before the first frame update
    void Start()
    {
        pm = GameObject.Find("FirstPersonPlayer").GetComponent<PlayerMovementScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if (slow)
        {
            pm.speed = 10f;
        } 
    }

    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            Debug.Log("Slow");
            slow = true;
        }


    }

    void OnTriggerExit(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {

            slow = false;
        }
    }
}
