﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DeadMenu : MonoBehaviour
{
    
    public bool PlayerAlive = true;
    
    public GameObject deadMenuUi;

    void Update()
    {
        if(PlayerAlive == false)
        {
            Dead();
        }
        else
        {
            Alive();
        }
      
    }


    public void Dead()
    {
        deadMenuUi.SetActive(true);
        Time.timeScale = 0f;
        PlayerAlive = false;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    public void Alive()
    {
        deadMenuUi.SetActive(false);
        Time.timeScale = 1f;
        PlayerAlive = true;
    }
    public void PlayGame()
    {
        PlayerAlive = true;
        deadMenuUi.SetActive(false);
        Time.timeScale = 1f;
        SceneManager.LoadScene("HumanEvolution");
    }

    public void LoadMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu");
        
        
    }
    public void QuitGame()
    {
        Debug.Log("Fechou o jogo");
        Application.Quit();
    }

}
