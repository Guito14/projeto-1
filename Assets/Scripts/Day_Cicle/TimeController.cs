﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeController : MonoBehaviour
{
    [SerializeField] private Light sun;

    [SerializeField] private float secondsInFullDay = 600f;

    [Range(0, 1)] [SerializeField] private float currentTimeOfDay = 0;
    private float timeMultiplayer = 1f;
    private float sunInitialIntensity;

    QuestNivel3 questNivel3;


    void Start()
    {
        questNivel3 = GameObject.Find("MongeNivel3").GetComponent<QuestNivel3>();
        sunInitialIntensity = sun.intensity;
        currentTimeOfDay = 0.63f;
    }


    void Update()
    {
        if (questNivel3.isQuestStart)
        {
            UpdateSun();


            currentTimeOfDay += (Time.deltaTime / secondsInFullDay) * timeMultiplayer;

            if (currentTimeOfDay >= 1)
            {

                currentTimeOfDay = 0;
            }
        }
    }

    void UpdateSun()
    {
        sun.transform.localRotation = Quaternion.Euler((currentTimeOfDay * 360f) - 90, 170, 0);

        float intensityMultiplier = 1;

        if (currentTimeOfDay <= 0.23f || currentTimeOfDay >= 0.75f)
        {
            intensityMultiplier = 0f;
        }
        //Nascer do Sun 
        else if (currentTimeOfDay <= 0.25f)
        {
            intensityMultiplier = Mathf.Clamp01((currentTimeOfDay - 0.23f) * (1 / 0.02f));
        }
        //Por-Do-Sol
        else if (currentTimeOfDay >= 0.73f)
        {
            intensityMultiplier = Mathf.Clamp01(1 - ((currentTimeOfDay - 0.73f) * (1 / 0.02f)));
        }
        sun.intensity = sunInitialIntensity * intensityMultiplier;
    }

}
