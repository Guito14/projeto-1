﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Cameras;


public class EnterBoat : MonoBehaviour
{
    public GameObject BoatCamMode, ThePlayer, TheBoat;
    public int TrigerCheck;

    public void Start()
    {
        TrigerCheck = 0;
    }
    public void OnTriggerEnter(Collider other)
    {
        TrigerCheck = 1;
    }

    public void OnTriggerExit(Collider other)
    {
        TrigerCheck = 0;
    }
    public void Update()
    {
        if (TrigerCheck == 1)
        {
            if (Input.GetButtonDown("Action"))
            {
                BoatCamMode.SetActive(true);
                ThePlayer.SetActive(false);
                TheBoat.GetComponent<BoatControllerScript>().enabled = true;
                TheBoat.GetComponent<BoatControllerScript>().enabled = true;

            }
        }
    }
}

