﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberOfDogsKilled : MonoBehaviour
{
    public GameObject dogIcon;
    void Start()
    {
        dogIcon.SetActive(false);
    }
    void Update()
    {
        GetComponent<Text>().text = "" + GameManager.instanciar.DogsKilled;

        if (GameManager.instanciar.DogsKilled < 1)
        {
            GetComponent<Text>().color = Color.red;
        }
        else if (GameManager.instanciar.DogsKilled < 2)
        {
            GetComponent<Text>().color = Color.yellow;
        }
        else if (GameManager.instanciar.DogsKilled >=3)
        {
            GetComponent<Text>().color = Color.green;
        }
        dogIcon.SetActive(true);
    }
}
