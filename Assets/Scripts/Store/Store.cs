﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Store : MonoBehaviour
{
    //[SerializeField]
    //private GameObject[] itemList;
    //private int itemNum;

    [SerializeField]
    GameObject lojaUI;

    [SerializeField]
    InventoryNew inv;

    public StoreUI storeUI;
    PickUpNew item;
   

    [SerializeField]
    PlayerMovementScript player;

    [SerializeField]
    bool canShop;

    private void Start()
    {

        player = GameObject.Find("FirstPersonPlayer").GetComponent<PlayerMovementScript>();
        canShop = false;
        lojaUI.SetActive(false);
        
    }

    public void Update()
    {
        if (canShop && Input.GetKeyDown(KeyCode.B))
        {
            lojaUI.SetActive(true);
        }




    }

   
    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            canShop = true;

        }
    }

    void OnTriggerExit(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {

            lojaUI.SetActive(false);
            canShop = false;
            storeUI.textCantBuy.SetActive(false);
        }
    }


}
