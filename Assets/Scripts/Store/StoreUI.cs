﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreUI : MonoBehaviour
{
    
   public GameObject textCantBuy;
   
    [SerializeField]
    private GameObject[] itemList;
    private int itemNum;

    [SerializeField]
    private List<Item> materiais;

    [SerializeField]
    InventoryNew inv;

    public bool machado = false;
    public bool torch = false;
    public bool faca = false;
    public bool bow = false;

    

    PickUpNew item;


    [SerializeField]
    PlayerMovementScript player;



    public void BuyAxe()
    {
       
            int madeira = 3;
            int pedra = 2;
            int liana = 2;

        if (pedra <= inv.howMany(materiais[0]) && madeira <= inv.howMany(materiais[1]) && liana <= inv.howMany(materiais[2]))
        {
            itemNum = 0;
            Instantiate(itemList[itemNum], player.transform.position, Quaternion.identity);
            inv.RemoveX_Items(pedra, materiais[0]);
            inv.RemoveX_Items(madeira, materiais[1]);
            inv.RemoveX_Items(liana, materiais[2]);
            machado = true;

        }
        else
        {
            textCantBuy.SetActive(true);
            Debug.Log("items insuficientes");
        }
        
    }
    public void BuyKnife()
    {
        int madeira = 2;
        int pedra = 1;
        int liana = 1;

        if (pedra <= inv.howMany(materiais[0]) && madeira <= inv.howMany(materiais[1]) && liana <= inv.howMany(materiais[2]))
        {
            itemNum = 1;
            Instantiate(itemList[itemNum], player.transform.position, Quaternion.identity);
            inv.RemoveX_Items(pedra, materiais[0]);
            inv.RemoveX_Items(madeira, materiais[1]);
            inv.RemoveX_Items(liana, materiais[2]);
            faca = true;
        }
        else
            Debug.Log("items insuficientes");
    }
    public void BuyBow()
    {
        int madeira = 4;
        int liana = 5;
        if (madeira <= inv.howMany(materiais[1]) && liana <= inv.howMany(materiais[2]))
        {
            itemNum = 2;
            Instantiate(itemList[itemNum], player.transform.position, Quaternion.identity);
            inv.RemoveX_Items(madeira, materiais[1]);
            inv.RemoveX_Items(liana, materiais[2]);
            bow = true;
        }
        else
            Debug.Log("items insuficientes");
    }
    public void BuyTorch()
    {
        int madeira = 5;
        if (madeira <= inv.howMany(materiais[1]) )
        {
            itemNum = 3;
            Instantiate(itemList[itemNum], player.transform.position, Quaternion.identity);
            inv.RemoveX_Items(madeira, materiais[1]);
            torch = true; 
        }
        else
            Debug.Log("items insuficientes");
    }
}
