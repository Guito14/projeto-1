﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quest_Nivel_4 : MonoBehaviour
{

    //public GameObject tabuas;
    public GameObject arrow;
    //public GameObject CUTSCENE;
    public GameObject TigersCount;
    public GameObject fishCount;

    public GameObject jangada;


    public GameObject fill;
    bool killedAll;
    bool isPressQuest;
   public bool fazerArco;
   public bool fished;
    public StoreUI store;
    public bool isQuestStart;
    public bool isQuestCompleted;

    GameManager gm_tiger;
    GameManager gm_fish;



    //variavel para usar variaveis do script DialogueSystem
    private DialogueSystem dialogueSystem;

    //Nome do NPC
    public string Name;

    //Mexer no tamanho da Area da Caixa de Texto, danos mais espaço para escrever nas frases
    [TextArea(5, 10)]

    public string[] phrases;

    void Start()
    {
        
        isQuestStart = false;
        killedAll = false;
        isQuestCompleted = false;
        //CUTSCENE.SetActive(false);
        TigersCount.SetActive(false);
        fishCount.SetActive(false);
        fazerArco = false;
        jangada.SetActive(false);
        gm_fish = GameObject.Find("GameManager").GetComponent<GameManager>();
        gm_tiger = GameObject.Find("GameManager").GetComponent<GameManager>();
        dialogueSystem = FindObjectOfType<DialogueSystem>();

        fill.SetActive(false);

    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.F) && isPressQuest)
        {

            //começou a quest
            isQuestStart = true;

            TigersCount.SetActive(true);
            fishCount.SetActive(true);

        }
       
        if (store.bow)
        {
            fazerArco = true;
        }
        if (gm_fish.fishcount >= 4)
        {
            fished = true;

        }
        if (gm_tiger.TigersKilled >= 5)
        {
            killedAll = true;
        }
        if(killedAll  && fazerArco && fished)
        {
            isQuestCompleted = true;
            dialogueSystem.DropDialogue();
            fill.SetActive(true);
            jangada.SetActive(true);
        }

    }


    public void OnTriggerStay(Collider other)
    {
        // ativar o script QuestNivel2
        // assim nao temos muitos scripts a correr ao mesmo tempo
        // e este só fica ativo quando precisamos
        this.gameObject.GetComponent<Quest_Nivel_4>().enabled = true;

        if ((other.gameObject.tag == "Player"))
        {
            FindObjectOfType<DialogueSystem>().EnterRangeOfNPC();
        }

        if ((other.gameObject.tag == "Player") && Input.GetKeyDown(KeyCode.F))
        {
            TigersCount.SetActive(true);
            isPressQuest = true;

            Destroy(arrow);

            this.gameObject.GetComponent<Quest_Nivel_4>().enabled = true;

            dialogueSystem.Names = Name;
            dialogueSystem.dialogueLines = phrases;
            FindObjectOfType<DialogueSystem>().NPCName();
        }
        if ((other.gameObject.tag == "Player" && isQuestCompleted))
        {
            isQuestCompleted = true;
            TigersCount.SetActive(false);
            fishCount.SetActive(false);


        }

    }

    public void OnTriggerExit()
    {
        if (isQuestStart)
        {


        }
        if (isQuestCompleted)
        {


        }



        FindObjectOfType<DialogueSystem>().OutOfRange();
        this.gameObject.GetComponent<Quest_Nivel_4>().enabled = false;
        isPressQuest = false;

    }







}

