﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberOfFished : MonoBehaviour
{ public GameObject fishIcon;
void Start()
{
    fishIcon.SetActive(false);
}
void Update()
{
    GetComponent<Text>().text = "" + GameManager.instanciar.fishcount;

    if (GameManager.instanciar.fishcount < 1)
    {
        GetComponent<Text>().color = Color.red;
    }
    else if (GameManager.instanciar.fishcount < 2)
    {
        GetComponent<Text>().color = Color.yellow;
    }
    else if (GameManager.instanciar.fishcount >= 3)
    {
        GetComponent<Text>().color = Color.green;
    }
    fishIcon.SetActive(true);
}
}