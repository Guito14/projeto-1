﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmsAnimation : MonoBehaviour
{

    private Animator animator;
    private bool isWalking;
    PlayerMovementScript player;
    float walk = 12f;
    PickUpNew pk;
    Rocha r;
    AudioManager am;

    SlotNew slot, slot1, slot2, slot3, slot4, slot5, slot6, slot7;
    // Start is called before the first frame update
    void Start()
    {
        slot = GameObject.Find("Slot").GetComponent<SlotNew>();
        slot2 = GameObject.Find("Slot (2)").GetComponent<SlotNew>();
        slot3 = GameObject.Find("Slot (3)").GetComponent<SlotNew>();
        slot4 = GameObject.Find("Slot (4)").GetComponent<SlotNew>();
        slot5 = GameObject.Find("Slot (5)").GetComponent<SlotNew>();
        slot6 = GameObject.Find("Slot (6)").GetComponent<SlotNew>();
        slot7 = GameObject.Find("Slot (7)").GetComponent<SlotNew>();
        slot1 = GameObject.Find("Slot (1)").GetComponent<SlotNew>();

        player = GameObject.Find("FirstPersonPlayer").GetComponent<PlayerMovementScript>();
        r = GameObject.Find("RockSmash").GetComponent<Rocha>();
        am = GameObject.Find("FirstPersonPlayer").GetComponent<AudioManager>();
        pk = GameObject.Find("RockSmash").GetComponent<PickUpNew>();
        animator = gameObject.GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        //Soco
        if (Input.GetKeyDown(KeyCode.Mouse0) && slot.itemtotal == 0 && slot1.itemtotal == 0 && slot2.itemtotal == 0 
                                              && slot3.itemtotal == 0 && slot4.itemtotal == 0 && slot5.itemtotal == 0 
                                              && slot6.itemtotal == 0 && slot7.itemtotal == 0)
        {
            am.Play("Punch");
            animator.SetTrigger("punch");
        }

        //Walk&Run mudar depois o if substituir o GetKey para receber o bool do isWalking do PlayerMovement
        if (Input.GetKey(KeyCode.W))
        {
            animator.SetBool("walk", true);
            animator.SetBool("idle", false);
            animator.speed = 0.75f;

            if (Input.GetKey(KeyCode.LeftShift))
            {
                animator.speed = 1.5f;
            }
            //if (player.speed == walk)
            // {
            //    animator.speed = 50.0f;
            //  }
            //else if (speed = crouchSpeed)
            //{
            //    animator.speed = 0.50f;
            //}
            //else if (speed = runningSpeed)
            //{
            //    animator.speed = 2f;
            //}
        }
        else
        {
            
            animator.speed = 1f;
            animator.SetBool("walk", false);
            animator.SetBool("idle", true);
        }



        //Rock Smashing Funciona quando o item esta no inventario tem que estar na mao para funcionar canSmash PickupNew
        if (Input.GetKeyDown(KeyCode.L) && r.canSmash && pk.estaNaMao && r.podePartir)
        {
            Debug.Log("Can SMASH");
            am.Play("CrackNuts");
            animator.SetTrigger("isSmashing");
        }
        //if (r.canSmash && pk.estaNaMao)
        //{
        //    Debug.Log("Can SMASH");
        //}
    }
}
