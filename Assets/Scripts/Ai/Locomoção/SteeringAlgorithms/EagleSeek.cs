﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Behaviour/EagleSeek")]
public class EagleSeek : SteeringBehaviour
{
    [SerializeField]
    float maxAccel = 3f;

    override public Steering GetSteering(MovementInfo npc, MovementInfo target)
    {

        // Direction vector, from npc to target
        Vector3 direction = target.position - npc.position;
        float distance = Vector3.Distance(target.position, npc.position);
        Steering steering = new Steering();

        if (distance > 13)
        {
            steering.linear = direction.normalized * maxAccel;
            //steering.linear.y = 0;
        }
        else
        {

        }
        return steering;
    }



}
