﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Behaviour/Seek")]
public class Seek : SteeringBehaviour
{
    [SerializeField]
    float maxAccel = 3f;
    float distance;

    override public Steering GetSteering(MovementInfo npc, MovementInfo target)
    {

        float distance = Vector3.Distance(target.position, npc.position);
        Steering steering = new Steering();
        // Direction vector, from npc to target
        if (distance < 50f)
        {
            Vector3 direction = target.position - npc.position;

            steering.linear = direction.normalized * maxAccel;
            steering.linear.y = 0;

            return steering;
        }
        return steering;
    }
}
