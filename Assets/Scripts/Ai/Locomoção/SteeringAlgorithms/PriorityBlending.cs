﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Behaviour/PriorityBlending")]
public class PriorityBlending : SteeringBehaviour
{
    [SerializeField] private List<WeightedBlending> priorityGroups;
    [SerializeField] private float epsilon = 0.0001f;

    public override SteeringBehaviour Init()
    {
        priorityGroups = new List<WeightedBlending>(priorityGroups.Select(x => Instantiate(x).Init() as WeightedBlending));
        return this;
    }

    public override Steering GetSteering(MovementInfo npc, MovementInfo target)
    {
        Steering steering;
        // Go through each priority group
        foreach (WeightedBlending group in priorityGroups)
        {
            // Run the group steering
            steering = group.GetSteering(npc, target);
            // Does this steering have a meaningful force.
            if (Mathf.Abs(steering.angular) > epsilon || steering.linear.magnitude > epsilon)
                return steering;
        }
        // Return nothing.
        return new Steering();
    }
}
