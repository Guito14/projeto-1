﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Behaviour/Wander")]
public class Wander : LookAt
{
    [SerializeField]
    float wanderOffset; // distance to circle center
    [SerializeField]
    float wanderRadius; // radius of the circle
    float wanderRate; // how much change direction?
    float wanderOrientation; // Where we are going to
    [SerializeField]
    float maxAcceleration;

    float RandomBinomial()
    {
        return Random.Range(-1f, 1f);
    }

    Vector3 Ang2vec(float alfa)
    {
        return new Vector3(Mathf.Cos(alfa), 0, Mathf.Sin(alfa));
    }


    public override Steering GetSteering(MovementInfo npc, MovementInfo target)
    {
        Steering steering = new Steering();
        target = new MovementInfo();
        wanderOrientation += RandomBinomial() * wanderRate;
        target.orientation = wanderOrientation + npc.orientation;

        target.position = npc.position;
        target.position += wanderOffset * Ang2vec(npc.orientation);
        target.position += wanderRadius * Ang2vec(target.orientation);

        steering = base.GetSteering(npc, target);
        steering.linear = maxAcceleration * Ang2vec(npc.orientation);
        return steering;

    }
}