﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICollisionDetector
{
    Collision GetCollision(Vector3 origin, Vector3 ray);
}
public class Collision
{
   public Vector3 point;
   public  Vector3 normal;
}