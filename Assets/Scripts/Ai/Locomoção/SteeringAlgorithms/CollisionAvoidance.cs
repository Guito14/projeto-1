﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Behaviour/CollisionAvoidance")]
public class CollisionAvoidance : SteeringBehaviour
{
    
    [SerializeField] float radius, maxAcceleration;

    [SerializeField] List<MovementInfo> targets;

    public override Steering GetSteering(MovementInfo npc ,MovementInfo target)
    {

        float shortestTime = float.MaxValue;
        MovementInfo firstTarget = null;
        float firstMinSeparation = 0f;
        Vector3 relativePos;
        Vector3 relativeVel;
        float relativeSpeed;
        float firstDistance = 0f;
        float timeToCollision;
        float minSeparation;
        Vector3 firstRelativePos = Vector3.zero;
        Vector3 firstRelativeVel = Vector3.zero;
        Steering steering = new Steering();

        foreach (MovementInfo target1 in targets)
        {
            relativePos = npc.position - target1.position;
            relativeVel = npc.velocity - target1.velocity;
            relativeSpeed = relativeVel.magnitude;
            timeToCollision = -Vector3.Dot(relativePos, relativeVel) /
            (relativeSpeed * relativeSpeed);
            float distance = relativePos.magnitude;
            minSeparation = distance - relativeSpeed * timeToCollision;
            if (minSeparation > 2 * radius) continue;
            if (timeToCollision > 0 && timeToCollision < shortestTime)
            {
                shortestTime = timeToCollision;
                firstTarget = target1;
                firstMinSeparation = minSeparation;
                firstDistance = distance;
                firstRelativePos = relativePos;
                firstRelativeVel = relativeVel;
            }
            //Steering steering = new Steering();
            if (firstTarget == null)
                return steering;

            if (firstMinSeparation <= 0 || firstDistance < 2 * radius)
            {
                relativePos = npc.position - firstTarget.position;
            }
            else
            {
                relativePos = firstRelativePos +
                firstRelativeVel * shortestTime;
            }

            relativePos.Normalize();
            steering.linear = relativePos * maxAcceleration;
        }
        return steering;
    }
}
