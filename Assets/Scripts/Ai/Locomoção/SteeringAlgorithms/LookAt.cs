﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Behaviour/LookAt")]

public class LookAt : Align
{
    //MovementInfo originalTarget;
    float epsilon = 0.0001f;


   public override Steering GetSteering(MovementInfo npc, MovementInfo target)
   {
        Steering steering = new Steering();
        //originalTarget = target;
        Vector3 direction =
        npc.position - target.position;
        if (direction.magnitude < epsilon)
            return new Steering();
        npc.orientation =
        Mathf.Atan2(direction.z, direction.x);
        return steering;
   }
}