﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Behaviour/ObstacleAvoidance")]
public class ObstacleAvoidance : Seek
{
    float avoidDistance, lookahead;
    ICollisionDetector CollDetector;
    

    public override Steering GetSteering(MovementInfo npc, MovementInfo target)
    {
        
        Vector3 rayVector = npc.velocity;
        rayVector.Normalize();
        rayVector *= lookahead;
        Collision collision =
        CollDetector.GetCollision(npc.position, rayVector);
        if (collision == null) return new Steering();
        target.position = collision.point;
        target.position += collision.normal * avoidDistance;
        return base.GetSteering(npc, target);
    }
}