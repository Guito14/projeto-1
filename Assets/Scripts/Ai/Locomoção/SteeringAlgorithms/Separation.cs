﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
[CreateAssetMenu(menuName = "Behaviour/Separation")]

public class Separation : SteeringBehaviour
{
    private List<MovementInfo> targetList;
    public float threshold = 3, k, maxAccell;

    public override SteeringBehaviour Init()
    {
        targetList = GameObject
            .FindGameObjectsWithTag("Dog")
            .Select(x => x.GetComponent<MovementManager>().GetInfo).ToList();
        return this;
    }

    public override Steering GetSteering(MovementInfo npc, MovementInfo ignoredTarget)
    {
        Steering steering = new Steering();
        foreach (MovementInfo target in targetList)
        {
            Vector3 direction = npc.position - target.position;
            float distanceSqr = direction.sqrMagnitude;
            if (distanceSqr < threshold * threshold)
            {
                float strength = Mathf.Min(maxAccell, k / distanceSqr);
                steering.linear += direction.normalized * strength;
            }
        }
        

        float sphereRadius = 0.3f;

        Collider[] collidersHit = Physics.OverlapSphere(npc.position, sphereRadius);


        foreach (Collider hit in collidersHit)
        {
            if (hit.gameObject.transform.position != npc.position)
            {

                steering.linear += npc.position - hit.transform.position;
                steering.linear.y *= 0f;
            }


        }

        return steering;
        
    }
}
