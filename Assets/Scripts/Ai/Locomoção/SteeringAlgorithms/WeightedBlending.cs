﻿using System;
using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Behaviour/WeightedBlending")]
public class WeightedBlending : SteeringBehaviour
{
    [SerializeField] private List<SteeringAndWeight> behaviours;
    [SerializeField] private float maxAcceleration;
    [SerializeField] private float maxRotation;

    public override SteeringBehaviour Init()
    {
        foreach (SteeringAndWeight b in behaviours)
        {
            b.behaviour = Instantiate(b.behaviour).Init();
        }
        return this;
    }

    public override Steering GetSteering(MovementInfo npc, MovementInfo target)
    {
        Steering steering = new Steering();
        float totalWeights = 0f;

        foreach (SteeringAndWeight behaviour in behaviours)
        {
            Steering tmp = behaviour.behaviour.GetSteering(npc, target);
            steering.linear += tmp.linear * behaviour.weight;
            steering.angular += tmp.angular * behaviour.weight;
            totalWeights += behaviour.weight;
        }
        // Media pesada
        steering.angular /= totalWeights;
        steering.linear /= totalWeights;
        // Nao ultrapassar limites de aceleração
        steering.linear = Vector3.ClampMagnitude(steering.linear, maxAcceleration);
        steering.angular = Mathf.Clamp(
            steering.angular, -maxRotation, maxRotation);
        return steering;
    }
}

