﻿using UnityEngine;

[CreateAssetMenu(menuName = "Behaviour/LookWhereYoureGoing")]

public class LookWhereYoureGoing : Align
{
    float epsilon = 0.00001f;
    private MovementInfo originalTarget;
    public override Steering GetSteering(MovementInfo npc, MovementInfo target)
    {
        if (npc.velocity.magnitude > epsilon) return new Steering();
        originalTarget = target;
        // set a temporary target
        target.orientation = Mathf.Atan2(-target.velocity.x, target.velocity.z);
        return base.GetSteering(npc, target);
    }
}
