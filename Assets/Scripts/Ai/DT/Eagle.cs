﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Eagle : MonoBehaviour
{
    /*              Hp < 30%
     *              /    \
     *            Yes       No
     *            /         \
     *          Run
     *                       distance < 40 do Player?                     
     *                           /          \                    
     *                           TRUE             FALSE
     *                                /               \
     *                          Distance < 10      Walking Arround
     *                           
     *                             /   \                 
     *                           Yes     NO            
     *                          /          \
     *                     HIT           Keeps Chasing until Distance > 40
     *                  
     *                  
     *                  
     *
     *
     *
     */
    public enum State
    {
        attack,
        chasing,
        walkArround,
        running,
    }

    State currentState;

    VitalsManager player;
    MovementManager npcMovement;
    float distance;

    //bool leavestate;

    EnemyHp hp;
    //public float howClose = 50.0f;

    // Condições
    DTCondition hp_30;
    DTCondition Distance_40;
    DTCondition Distance_10;


    [SerializeField]
    float damage;



    void Awake()
    {
        player = GameObject.Find("FirstPersonPlayer").GetComponent<VitalsManager>();
        hp = GetComponent<EnemyHp>();
        npcMovement = GetComponent<MovementManager>();
        distance = Vector3.Distance(this.transform.position, player.transform.position);


        //DTAction _waits = new DTAction(() => HoldASecond());
        DTAction _hit = new DTAction(() => Ataque());
        DTAction _walk = new DTAction(() => AndarRandom());
        DTAction _chase = new DTAction(() => Perseguir());
        DTAction _ghost = new DTAction(() => Fugir());


        Distance_10 = new DTCondition(() => distance < 15, _hit, _chase);
        Distance_40 = new DTCondition(() => distance < 100, Distance_10, _walk);
        hp_30 = new DTCondition(() => hp.hp < 30.0f, _ghost, Distance_40);



    }

    void Update()
    {
        distance = Vector3.Distance(this.transform.position, player.transform.position);
        //npcMovement.info.position.y = 110;
        hp_30.Run();

        switch (currentState)
        {
            case State.chasing:
                Perseguir();
                break;

            case State.walkArround:
                AndarRandom();

                break;

            case State.running:
                Fugir();
                break;

            default:
                break;
        }


        if (distance <= 5 && Input.GetKeyDown(KeyCode.Mouse0))
        {
            hp.hp -= 40f;

        }


    }


    public void Ataque()
    {
        player.currentHealth -= damage;

    }
    public void Perseguir()
    {
        foreach (SteeringBehaviour item in npcMovement.lista)
        {
            if (item.name == "Eagle Seek")
            {
                npcMovement.steeringBehaviour = item;
            }
        }
    }
    public void AndarRandom()
    {
        foreach (SteeringBehaviour item in npcMovement.lista)
        {
            //nome para random
            if (item.name == "Wander")
            {
                npcMovement.steeringBehaviour = item;
            }
        }
    }
    public void Fugir()
    {
        foreach (SteeringBehaviour item in npcMovement.lista)
        {
            //nome para fugir
            if (item.name == "Flee")
            {
                npcMovement.steeringBehaviour = item;
            }
        }
    }



    //IEnumerator HoldASecond()
    //{

    //    yield return new WaitForSeconds(3);
    //    leavestate = false;

    //}

    public void ChangeState(State st)
    {
        if (st != currentState)
        {
            currentState = st;
        }
    }



}
