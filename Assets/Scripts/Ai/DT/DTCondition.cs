﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DTCondition : DTNode
{
    private DTNode trueChild, falseChild;
    private Func<bool> condition;

    public DTCondition(Func<bool> condition, DTNode tChild, DTNode fChild)
    {
        this.condition = condition;
        trueChild = tChild;
        falseChild = fChild;
    }

    public override void Run()
    {
        if (condition()) trueChild.Run();
        else falseChild.Run();
    }
}
