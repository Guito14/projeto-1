﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TailAttackNode : BTNode
{
    private NavMeshAgent agent;
    private BehaviorTree t;
    

    

    public TailAttackNode(NavMeshAgent agent, BehaviorTree t)
    {
        this.agent = agent;
        this.t = t;
    }


    public override Resultado Run()
    {
        agent.isStopped = true;
        TailAnim();
        return Resultado.Running;
        
    }
    public void TailAnim()
    {
        Debug.Log("Tail Attack Anim");
    }

}