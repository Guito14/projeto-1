﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WanderNode : BTNode
{

    private Transform target;
    private NavMeshAgent agent;
    private BehaviorTree t;


    public WanderNode(Transform target, NavMeshAgent agent, BehaviorTree t)
    {
        this.target = target;
        this.agent = agent;
        this.t = t;
    }

    public override Resultado Run()
    {
        float distancia = Vector3.Distance(target.position, agent.transform.position);

        if (distancia >= 150f)
        {
            Debug.Log("Wander");
            return Resultado.Running;

        }
        else
        {
            agent.isStopped = true;
            return Resultado.Success;
        }
    }
}
