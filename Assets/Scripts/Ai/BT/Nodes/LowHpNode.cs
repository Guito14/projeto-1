﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LowHpNode : BTNode
{
    private BehaviorTree t;
    private float treshold;


    public LowHpNode(BehaviorTree t, float treshold)
    {
        this.t = t;
        this.treshold = treshold;
    }


    public override Resultado Run()
    {



        if (t.currentHp <= treshold)
        {

            Debug.Log("Low hp!!");
            return Resultado.Success;
        }
        else
        {
           // Debug.Log("High hp!!");
            return Resultado.Fail;
        }



    }
  
}
