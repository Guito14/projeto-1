﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegenHpNode : BTNode
{
    BehaviorTree hpAtual;
    BehaviorTree regenHpRate;
    private float currentHealth;
    
    public RegenHpNode(BehaviorTree regenHpRate, BehaviorTree hpAtual, float currentHealth)
    {
        this.currentHealth = currentHealth;
        this.regenHpRate = regenHpRate;
        this.hpAtual = hpAtual;

    }




    public override Resultado Run()
    {

        if (hpAtual.currentHp <= currentHealth)
        {
            hpAtual.currentHp += Time.deltaTime * regenHpRate.restoreHpSpeed;
            Debug.Log("Regenerando hp");
            return Resultado.Success;
        }
        else
        {
            return Resultado.Fail;
        }


    }
}
