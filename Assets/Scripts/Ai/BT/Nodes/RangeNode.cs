﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeNode : BTNode
{

     private float range;
    private Transform target;
    private Transform origin;

    public RangeNode( float range, Transform target, Transform origin)
    {
        this.range = range;
        this.target = target;
        this.origin = origin;

    }



    public override Resultado Run()
    {
        float distancia = Vector3.Distance(target.position, origin.position);

        if (distancia <= range)
        {
            return Resultado.Success;
        }
        else
        {
            return Resultado.Fail;
        }


    }
}
