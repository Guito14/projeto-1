﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighHpNode : BTNode
{
    private BehaviorTree t;
    private float limite;


    public HighHpNode(BehaviorTree t, float limite)
    {
        this.t = t;
        this.limite = limite;
    }


    public override Resultado Run()
    {



        if (t.currentHp >= limite)
        {
            Debug.Log("High hp!!");
            return Resultado.Success;
        }    
        else
        {
            
            return Resultado.Fail;
        }



    }

}