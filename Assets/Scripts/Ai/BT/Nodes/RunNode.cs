﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RunNode : BTNode
{
    private Transform target;
    private NavMeshAgent agent;
    private BehaviorTree t;


    public RunNode(Transform target, NavMeshAgent agent, BehaviorTree t)
    {
        this.target = target;
        this.agent = agent;
        this.t = t;
    }

    public override Resultado Run()
    {
        t.SetColor(Color.black);
        float distancia = Vector3.Distance(target.position, agent.transform.position);

        if (distancia > 0.5f)
        {
            Debug.Log("FUgindo");
            agent.isStopped = false;
            agent.SetDestination(-target.position);

            if (distancia >= 150f)
            {
                agent.isStopped = true;
            }

            return Resultado.Running;

        }
        else
        {
            agent.isStopped = true;
            return Resultado.Success;
        }
    }
}
