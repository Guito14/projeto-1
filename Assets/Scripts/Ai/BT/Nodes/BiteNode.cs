﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BiteNode : BTNode
{
    Transform transform;
    private NavMeshAgent agent;
    private BehaviorTree t;

    public BiteNode(NavMeshAgent agent, BehaviorTree t)
    {
        this.agent = agent;
        this.t = t;
    }
    

    public override Resultado Run()
    {
        agent.isStopped = true;
        BiteAnim();
        return Resultado.Running;
    }

    public void BiteAnim()
    {


        //fazer uma coroutine para executar a animação de x em x segundos.
        agent.transform.Rotate(new Vector3(0f, 200f, 0f) * Time.deltaTime);
        Debug.Log("Bite Anim");

       
    }

}
