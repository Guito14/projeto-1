﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTSequence : BTNode
{

    protected List<BTNode> nodes = new List<BTNode>();

    public BTSequence(List<BTNode> nodes)
    {
        this.nodes = nodes;
    }

    public override Resultado Run()
    {
        bool isAnyNodeRunning = false;

        foreach (var node in nodes)
        {
            switch (node.Run())
            {

                case Resultado.Running:
                    isAnyNodeRunning = true;
                    break;
                case Resultado.Success:
                    break;
                case Resultado.Fail:
                    _estado = Resultado.Fail;
                    return _estado;
                default:
                    break;

            }
        }


        _estado = isAnyNodeRunning ? Resultado.Running : Resultado.Success;
        return _estado;



        //if (_estado = isAnyNodeRunning)
        //    _estado = Resultado.Running;
        //else
        //    _estado = Resultado.Success;

    }






    //private int currentNode = 0;

    //public BTSequence(BehaviorTree t, BTNode [] children) : base (t, children)
    //{

    //}

    //public override Resultado Run()
    //{

    //    // se uma folha der return Fail, volta ao inicio. Para percorrer todos os filhos estes teem que dar sempre return Success :AND:
    //                                                           //
    //    if (currentNode < Children.Count)                      //
    //    {                                                      //
    //        Resultado resultado = Children[currentNode].Run(); //
    //                                                           //
    //                                                           //
    //        if (resultado == Resultado.Running)                //
    //        {                                                  //
    //            return Resultado.Running;                      //
    //        }                                                  //
    //        else if (resultado == Resultado.Fail)              //
    //        {                                                  //
    //            currentNode = 0;                               //
    //            return Resultado.Fail;                         //   SEQUENCIA
    //        }                                                  //
    //        else                                               //
    //        {                                                  //
    //            currentNode++;                                 //
    //            if (currentNode < Children.Count)              //
    //            {                                              //
    //                return Resultado.Running;                  //
    //            }                                              //
    //            else                                           //
    //            {                                              //
    //                currentNode = 0;                           //
    //                return Resultado.Success;                  //
    //            }                                              //
    //        }                                                  //
    //                                                           //
    //                                                           //
    //    }                                                      //
    //    return Resultado.Success;
    //}
}
