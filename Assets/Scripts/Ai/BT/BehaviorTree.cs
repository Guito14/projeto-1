﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class BehaviorTree : MonoBehaviour
{

    [SerializeField] private float startHp;
    [SerializeField] private float limiteLow;
    [SerializeField] private float limiteHigh;
    [SerializeField] public float restoreHpSpeed;

    [SerializeField] private float visibleRange;
    [SerializeField] private float chasingRange;
    [SerializeField] private float biteRange;
    [SerializeField] private float tailAttackRange;
   

    [SerializeField] private float largeRange;

    [SerializeField] private Transform playerTransform;

    private Material material;
    public NavMeshAgent agent;

    private float nextActionTime = 5.0f;
    public float period = 5.1f;

    private BTNode topNode;

    VitalsManager player;
    [SerializeField] float damage;
    private float _currentHp;

    public float currentHp
    {
        get { return _currentHp; }
        set { _currentHp = Mathf.Clamp(value, 0, startHp); }
    }

    void Awake()
    {
        agent.GetComponent<NavMeshAgent>();
        material = GetComponent<MeshRenderer>().material;
        
    }

    private void Start()
    {
        _currentHp = startHp;
        player = GameObject.Find("FirstPersonPlayer").GetComponent<VitalsManager>();
        ConstructTree();
    }

    private void ConstructTree()
    {


        WanderNode wanderNode = new WanderNode(playerTransform, agent, this); //vaguear
        RegenHpNode regenHpNode = new RegenHpNode(this, this, currentHp); // regen hp
        RangeNode visibleNode = new RangeNode(visibleRange, playerTransform, transform);  // visible
        BiggerRangeNode largeRangeNode = new BiggerRangeNode(largeRange, playerTransform, transform); //large range
        ScreamNode screamNode = new ScreamNode(); // scream
        RangeNode chasingRangeNode = new RangeNode(chasingRange, playerTransform, transform); //check range to seek
        ChaseNode chaseNode = new ChaseNode(playerTransform, agent, this);//seek
        RangeNode atackRangeNode = new RangeNode(biteRange, playerTransform, transform); //Close Range x2
        BiteNode biteNode = new BiteNode(agent, this); // Bite
        LowHpNode lowHpNode = new LowHpNode(this, limiteLow); //hp<50
        HighHpNode highHpNode = new HighHpNode(this, limiteHigh); //hp>50
        TailAttackNode tailAttackNode = new TailAttackNode(agent, this);//Tail Attack
        RunNode runNode = new RunNode(playerTransform, agent, this); //run


        BTSelector ataquesSelector = new BTSelector(new List<BTNode> { biteNode, tailAttackNode });
        BTSequence closeRangeHighHpSequence = new BTSequence(new List<BTNode> { atackRangeNode, highHpNode, ataquesSelector });
        //BTDecorator vaguearDecorator = new BTDecorator(new BTNode { wanderNode });
        BTSelector fugindoSequence = new BTSelector(new List<BTNode> { regenHpNode, runNode });
        BTSequence closeRangeLowHpSequence = new BTSequence(new List<BTNode> { atackRangeNode, lowHpNode, fugindoSequence});
        BTSelector closeRangeActionsSelector = new BTSelector(new List<BTNode> { closeRangeLowHpSequence, closeRangeHighHpSequence});
        BTSequence largeRangeActionsSequence = new BTSequence(new List<BTNode> {visibleNode, largeRangeNode, screamNode, chasingRangeNode, chaseNode });

        topNode = new BTSelector(new List<BTNode> { largeRangeActionsSequence, closeRangeActionsSelector });


    }



    public void Update()
    {


        if (Time.time > nextActionTime)
        {
            nextActionTime = Time.time + period;
            topNode.Run();
        }

        //if (Time.time > nextActionTime)
        //{
        //    nextActionTime += period;
        //    topNode.Run();
        //    // execute block of code here
        //}
        

        if (topNode.estado == BTNode.Resultado.Fail)
        {

            Debug.Log("Nothing going on");

            //SetColor(Color.red);
        }
        //if (topNode.estado == BiteNode.Resultado.Running)
        //{
        //    player.currentHealth =- damage;
        //    Debug.Log("asdafdefs");
                
        //}
      


    }




    public void SetColor(Color color)
    {
        material.color = color;
    }


    //private BTNode mRoot;
    //private bool startedBehavior;
    //private Coroutine behaviour;

    //public BTNode Root { get { return mRoot; } }


    //MovementManager npcMovement;


    //// Start is called before the first frame update
    //void Start()
    //{


    //    //primeiro behavior stopped
    //    startedBehavior = false;

    //    npcMovement = GetComponent<MovementManager>();


    //    mRoot = new BTSequence(this, new BTNode[] {  } );

    //}

    //// Update is called once per frame
    //void Update()
    //{
    //    if (!startedBehavior)
    //    {
    //        behaviour = StartCoroutine(RunBehavior());
    //        startedBehavior = true;

    //    }

    //}


    //private IEnumerator RunBehavior()
    //{
    //    BTNode.Resultado resultado = Root.Run();

    //    while (resultado == BTNode.Resultado.Running)
    //    {
    //        Debug.Log("Resultado Root: " + resultado);
    //        yield return null;
    //        resultado = Root.Run();

    //    }

    //    Debug.Log("Comportamento terminou com:" + resultado);
    //}

    //public void Fugir()
    //{
    //    foreach (SteeringBehaviour item in npcMovement.lista)
    //    {
    //        //nome para fugir
    //        if (item.name == "Flee")
    //        {
    //            npcMovement.steeringBehaviour = item;
    //        }
    //    }
    //}

    //public void Perseguir()
    //{
    //    foreach (SteeringBehaviour item in npcMovement.lista)
    //    {
    //        if (item.name == "Seek")
    //        {
    //            npcMovement.steeringBehaviour = item;
    //        }
    //    }
    //}
}
