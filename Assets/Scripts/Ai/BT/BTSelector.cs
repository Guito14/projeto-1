﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class BTSelector : BTNode
{

    protected List<BTNode> nodes = new List<BTNode>();

    public BTSelector(List<BTNode> nodes)
    {
        this.nodes = nodes;
    }

    public override Resultado Run()
    {

        foreach (var node in nodes)
        {
            switch (node.Run())
            {

                case Resultado.Running:
                    _estado = Resultado.Running;
                    return _estado;
                case Resultado.Success:
                    _estado = Resultado.Success;
                    return _estado;
                case Resultado.Fail:
                default:
                    break;

            }
        }
        _estado = Resultado.Fail;
        return _estado;

    }


    
}


    //private int currentNode = 0;

    //public BTSelector(BehaviorTree t, BTNode[] children) : base(t, children)
    //{

    //}
    
    //public override Resultado Run()
    //{
    //    //se uma folha der return fail, passa para a proxima. Basta uma ser Success para dar return Success. Return fail quando todos os resultados dao fail :OR:
                                                                                                                       
    //    if (currentNode < Children.Count)                                                                              
    //    {                                                                                                      //        
    //        Resultado resultado = Children[currentNode].Run();                                                 //        
    //                                                                                                           //        
    //        if (resultado == Resultado.Running)                                                                //        
    //        {                                                                                                  //        
    //            return Resultado.Running;                                                                      //        
    //        }                                                                                                  //        
    //        else if (resultado == Resultado.Success) //da return success se 1 for success e passa ao proximo   //      
    //        {                                                                                                  //        SELETOR
    //            currentNode = 0;                                                                               //        
    //            return Resultado.Success;                                                                      //        
    //                                                                                                           //        
    //        }                                                                                                  //        
    //        else //resultado fail  - passa ao proximo e se todos forem fail return fail                        //        
    //        {                                                                                                  //        
    //            currentNode++;                                                                                 //        
    //            if (currentNode < Children.Count)                                                              //        
    //            {                                                                                              //        
    //                return Resultado.Running;                                                                  //        
    //            }                                                                                              //        
    //            else //se todos derem fail return fail                                                         //        
    //            {                                                                                              //        
    //                currentNode = 0;                                                                           //        
    //                return Resultado.Fail;                                                                     //        
    //            }                                                                                              //
    //        }                                                                                                  //


    //    }
    //    return Resultado.Success;
    //}

