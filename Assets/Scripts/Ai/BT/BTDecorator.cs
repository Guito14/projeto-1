﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTDecorator : BTNode
{
    protected BTNode node;

    public BTDecorator(BTNode node)
    {
        this.node = node;
    }

    public override Resultado Run()
    {
         switch (node.Run())
         {

                case Resultado.Running:
                    _estado = Resultado.Running;
                     break;
                case Resultado.Success:
                    _estado = Resultado.Success;
                     break;
                case Resultado.Fail:
                    _estado = Resultado.Fail;
                     break;
                default:
                     break;

         }
        return _estado;

    }
}
