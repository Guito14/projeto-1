﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Croc : MonoBehaviour
{
    EnemyHp hp;
    public float howClose = 50.0f;
    private float moveSpeed = 3;
    public float distance;
    private Transform player;
    public float crocDamage = 40f;
    [SerializeField]
    private PickUpNew pk;
    VitalsManager vm;
    int time_lost = 0;
    // Start is called before the first frame update
    void Start()
    {
        vm = GameObject.Find("FirstPersonPlayer").GetComponent<VitalsManager>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(player.position, transform.position);
        if (distance <= howClose)
        {
           
            GetComponent<Rigidbody>().AddForce(transform.forward * moveSpeed);
            
            if (distance <= 10)
            {

                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    hp.hp -= 5.0f;
                }
            }
        }

        if (distance <= 10)
        {

            if (time_lost++ == 10) //CUSTOM TIMER
            {
                time_lost = 0;
                vm.TakeDamage(crocDamage);

            }

            
        }
    }
}
