﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BTNode 
{
    protected Resultado _estado;

    public Resultado estado { get { return _estado; } }

    public abstract Resultado Run();
  

    public enum Resultado { Running, Success, Fail };
}
