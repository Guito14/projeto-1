﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JangadaAnim : MonoBehaviour
{

    private Animator animator;
    bool jangadaAtive = false;
    // Start is called before the first frame update
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L) && jangadaAtive)
        {
            animator.SetTrigger("Iscollidingjangada");
            animator.SetFloat("Speed", 0.5f);
        }

        if (Input.GetKeyDown(KeyCode.M) && jangadaAtive)
        {
            animator.SetTrigger("Iscollidingjangada");
            animator.SetFloat("Speed", -0.5f);
        }
    }

    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            jangadaAtive = true;
           
        }


    }

    void OnTriggerExit(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            jangadaAtive = false;
           //GetComponent<Animator>().enabled = true;
        

        }
    }


}
