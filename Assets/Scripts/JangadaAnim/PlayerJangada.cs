﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJangada : MonoBehaviour
{
    private Animator animator;
    bool playerAtive = false;
    bool s;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Animator>().enabled = false;
        animator = gameObject.GetComponent<Animator>();

        s = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L) && playerAtive)
        {
            GetComponent<Animator>().enabled = true;
            animator.SetTrigger("FpsCollJangada");
            animator.SetFloat("SpeedPlayer", 0.5f);
            
        }

        if (Input.GetKeyDown(KeyCode.M) && playerAtive)
        {
            GetComponent<Animator>().enabled = true;
            animator.SetTrigger("FpsCollJangada");
            animator.SetFloat("SpeedPlayer", -0.5f);
        }
    }

   

    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Jangada")
        {
            playerAtive = true;
            
        }

    }

    void OnTriggerExit(Collider hit)
    {
        if (hit.gameObject.tag == "Jangada")
        {
            playerAtive = false;
           

        }
    }

    public void End6to5(string message)
    {
        if (message.Equals("start"))
        {

            if (s == true)
            {
                Debug.Log("Start");
                GetComponent<Animator>().enabled = false;
            }




        }
    }

    public void End5to6(string message)
    {
        
        if (message.Equals("finish"))
        {
            Debug.Log("finish");
            GetComponent<Animator>().enabled = false;
           s = true;
        }

    }
    
   
}
