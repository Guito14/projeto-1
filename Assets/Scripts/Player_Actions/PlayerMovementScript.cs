﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class PlayerMovementScript : MonoBehaviour
{

    [SerializeField]
    public CharacterController controller;
    public ParticleSystem particleLauncher;
    public CharacterController characterCollider;
    [SerializeField]
    MovementInfo info;

    [SerializeField]
    public float Health = 100f;
    public float speed = 50f;
    public float gravity = -9.81f;
    public float jumpHeigth = 3f;
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    public float heigth = 1.8f;
    private Vector3 velocity;
    private int time_lose, time_lost = 0;

    AudioManager am;

    [SerializeField]
    public bool drinking = false;
    public bool fishing = false;
    private bool invoked = true;
    private bool invoked_complete = false;
    private bool isGrounded;

    public MovementInfo GetInfo => info;
    //[SerializeField]
    //float linearDrag = 0.95f;
    //[SerializeField]
    //float angularDrag = 0.95f;
    //[SerializeField]
    //float maxVelocity = 20f;
    //[SerializeField]
    //float rotationVelocity = Mathf.PI / 8f;
    //[SerializeField]
    //float walkVelocity = 8f;


    //public bool isWalking = false;




    [SerializeField]
    private QuestNivel1 questNivel1;
    //private QuestNivel3 questNivel3;
    //private QuestNivel2 questNivel2;
    VitalsManager vitalsManager;

    //public GameObject playerUpgrade_Hud;

    //public GenerateDogs[] generateDogs;


    //start
    void Start()
    {
        characterCollider = gameObject.GetComponent<CharacterController>();
        vitalsManager = GameObject.Find("FirstPersonPlayer").GetComponent<VitalsManager>();
        characterCollider.height = 3.8f;
        // questmaca = GameObject.Find("Monge_Lvl_1").GetComponent<questMaca>();
        questNivel1 = GameObject.Find("Monge_Lvl_1").GetComponent<QuestNivel1>();
        //playerUpgrade_Hud.SetActive(false);
        am = GameObject.Find("FirstPersonPlayer").GetComponent<AudioManager>();

        //MovimentInfo -Position and orientation
        info.position = transform.position;
        Vector3 forward = transform.forward;
        info.orientation = Mathf.Atan2(forward.x, forward.z);
        info.velocity = velocity;


       
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) ||
           Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D))
        {
            am.Play("Walking");
        }
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");


        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.deltaTime);
        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);


        info.position = transform.position;
        Vector3 forward = transform.forward;
        info.orientation = Mathf.Atan2(forward.x, forward.z);

        info.velocity = velocity;

        //saltar
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            am.Play("Jump");
            velocity.y = Mathf.Sqrt(jumpHeigth * -2f * gravity);
        }

        

        //Crouch
        if (Input.GetKey(KeyCode.LeftControl))
        {
            characterCollider.height = 1.9f;
            speed = 8f;
        }
        else
        {
            characterCollider.height = 3.8f;
            speed = 12f;
        }

      
        //Crouch ao beber
        if (drinking || fishing)
        {
            characterCollider.height = 1.9f;
            speed = 8f;
        }
        else if (!Input.GetKey(KeyCode.LeftControl))
        {
            characterCollider.height = 3.8f;
            speed = 12f;
        }


        //Ao acabar a quest dar upgrade ao player e destruir os cães
        if (questNivel1.isQuestStart == true)
        {

            //verificar se a quest esta completa
            if (questNivel1.isFirstQuestCompleted == true)
            {


                if (time_lost++ == 500) //CUSTOM TIMER
                {
                    time_lost = 0;
                    //playerUpgrade_Hud.SetActive(false);
                }
                if (!invoked_complete)
                {
                    //playerUpgrade_Hud.SetActive(true);
                    invoked_complete = true;
                    Invoke("ParticleLaunch", 1);
                    Invoke("ParticleLaunch", 2);
                    Invoke("ParticleLaunch", 3);
                };
            }
            //if (questNivel2.isQuestCompleted)
            //{


            //}
            //if (questNivel3.isQuestCompleted)
            //{


            //}
        }

        Sprint();


        ////Movement Animations
        //if (speed > 0)
        //    isWalking = true;


        

    }

 


    void Sprint()
    {
        float z = Input.GetAxis("Vertical");

        if (Input.GetKey(KeyCode.LeftShift) && z == 1 && vitalsManager.currentThirsth > 0)
        {


            speed = 25.0f;

            if (invoked)
            {
                vitalsManager.currentThirsth = vitalsManager.currentThirsth - 0.5f;
                invoked = false;
            }

            if (time_lose++ == 100)
            {
                time_lose = 0;
                invoked = true;
            }


            if (vitalsManager.currentThirsth <= 0)
            {
                vitalsManager.currentThirsth = 0;
                speed = 12f;
            }
            //Mensagem a dizer que nao da para correr sem agua ??
        }
    }
    public void ParticleLaunch()
    {
        particleLauncher.Emit(10); //emite salpicos

    }
    
    public void SavePlayer()
    {

        SaveSystem.SavePlayer(this, vitalsManager);

    }
    //Função de Load do Player
    public void LoadPlayer()
    {

        PlayerData data = SaveSystem.LoadPlayer();
        vitalsManager.currentHealth = data.health;
        vitalsManager.currentThirsth = data.thirst;
        vitalsManager.currentHunger = data.hunger;

        Vector3 position;
        position.x = data.position[0];
        position.y = data.position[1];
        position.z = data.position[2];

    }



}
