﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Bow : MonoBehaviour
{
    public GameObject arrowPrefab;

    public Transform destination;

    public float arrowSpeed = 30;

    public float lifeTime = 3;

    public bool canShoot;

    public int ammo = 10;

    public InventoryNew inv;

    public float fireRate = 2.0f;
    private float nextFire = 1.5F;

    public bool trigerExit = false;


    // Start is called before the first frame update
    void Start()
    {
        canShoot = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && gameObject.tag == "Bow" && canShoot && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Fire();
           
 
        }
       
    }
    private void Fire()
    {
        GameObject arrow = Instantiate(arrowPrefab);

        Physics.IgnoreCollision(arrow.GetComponent<Collider>(),
        destination.parent.GetComponent<Collider>());

        arrow.transform.position = destination.position;

        Vector3 rotation = arrow.transform.rotation.eulerAngles;

        arrow.transform.rotation = Quaternion.Euler(rotation.x, transform.eulerAngles.y, rotation.z);

        arrow.GetComponent<Rigidbody>().AddForce(destination.forward * arrowSpeed, ForceMode.Impulse);

        StartCoroutine(DestroyArrowAfterTime(arrow, lifeTime));
        DelayArrowShoot();
    }
    //private void Fire()
    //{
    //    for (int i = 0; i < inv.itemsList.Count; i++)
    //    {
    //        if(inv.itemsList[i].name == "Arrow")
    //        {
    //            GameObject arrow = Instantiate(arrowPrefab);

    //            Physics.IgnoreCollision(arrow.GetComponent<Collider>(),
    //                destination.parent.GetComponent<Collider>());

    //            arrow.transform.position = destination.position;

    //            Vector3 rotation = arrow.transform.rotation.eulerAngles;

    //            arrow.transform.rotation = Quaternion.Euler(rotation.x, transform.eulerAngles.y, rotation.z);

    //            arrow.GetComponent<Rigidbody>().AddForce(destination.forward * arrowSpeed, ForceMode.Impulse);

    //            StartCoroutine(DestroyArrowAfterTime(arrow, lifeTime));
    //            DelayArrowShoot();

    //            inv.RemoveItem(inv.itemsList[i]);
    //        }

    //    }

    //}

    private IEnumerator DestroyArrowAfterTime(GameObject arrow, float delay)
    {
        yield return new WaitForSeconds(delay);

        Destroy(arrow);
    }
    private IEnumerator DelayArrowShoot()
    {
        yield return new WaitForSeconds(100f);

    }

    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            canShoot = true;

        }
    }

    void OnTriggerExit(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            canShoot = false;
        }
    }
}
