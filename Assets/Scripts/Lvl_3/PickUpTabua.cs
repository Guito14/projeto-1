﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpTabua : MonoBehaviour
{

    public GameObject text_apanharArma;

    public bool apanharArma;
    public bool armaApanhada;
    public bool estaNaMao;
    
    public Transform interationZone;
    public GameObject Destination;


    void Start()
    {
        text_apanharArma.SetActive(false);
    }
    void Update()
    {


        if (Input.GetKeyDown(KeyCode.E) && apanharArma && !armaApanhada)
        {

            estaNaMao = true;
            apanharArma = false;
            this.transform.SetParent(interationZone);
            this.transform.position = Destination.transform.position;
            this.transform.rotation = Destination.transform.rotation;
            this.GetComponent<Rigidbody>().useGravity = false;
            this.GetComponent<Rigidbody>().isKinematic = true;
            gameObject.SetActive(true);
        }
        if (estaNaMao)
        {
            apanharArma = false;
            if (Input.GetKeyDown(KeyCode.Mouse0))
                transform.Rotate(Vector3.up * 150 * Time.deltaTime);
            if (Input.GetKeyDown(KeyCode.Mouse1))
                transform.Rotate(-Vector3.up * 150  * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.G) && estaNaMao)
        {
            this.transform.SetParent(null);
            this.GetComponent<Rigidbody>().useGravity = true;
            this.GetComponent<Rigidbody>().isKinematic = false;
            this.apanharArma = false;
            this.armaApanhada = false;
           
            estaNaMao = false;
        }

    }
    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            apanharArma = true;
            text_apanharArma.SetActive(true);
        }

    }

    void OnTriggerExit(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            apanharArma = false;
            text_apanharArma.SetActive(false);
            armaApanhada = false;
        }
    }
}
