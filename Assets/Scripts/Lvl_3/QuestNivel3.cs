﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;

//public class QuestNivel3 : MonoBehaviour
//{
//    //public GameObject Hud_Quest3Started;
//    //public GameObject Hud_Quest3Finished;


//    bool isPressQuest;

//    public bool isQuestStart;
//    public bool isQuestCompleted;
//    public bool fished;
//    public GameObject DogsCount;


//    void Start()
//    {
//        isQuestStart = false;
//        DogsCount.SetActive(false);

//        //Hud_Quest3Started.SetActive(false);
//        //Hud_Quest3Finished.SetActive(false);
//        fished = false;


//    }

//    void Update()
//    {

//        if (Input.GetKeyDown(KeyCode.F) && isPressQuest)
//        {
//          //  Hud_Quest3Started.SetActive(true);
//            //começou a quest
//            isQuestStart = true;
//            DogsCount.SetActive(true);

//        }
//        if (isQuestStart && fished)
//        {

//           // Hud_Quest3Finished.SetActive(true);

//        }


//    }

//    void OnTriggerEnter(Collider hit)
//    {
//        if (hit.gameObject.tag == "Player")
//        {
//            isPressQuest = true;
//        }

//        if (hit.gameObject.tag == "Player" && isQuestCompleted)
//        {
//            isPressQuest = true;
//            //  Hud_Quest3Finished.SetActive(true);
//            isQuestCompleted = true;
//            DogsCount.SetActive(false);
//        }
//    }


//    void OnTriggerExit(Collider hit)
//    {
//        if (hit.gameObject.tag == "Player")
//        {
//            //Hud_Quest3Started.SetActive(false);
//            isPressQuest = false;
//           //Hud_Quest3Finished.SetActive(false);
//        }
//    }
//}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestNivel3 : MonoBehaviour
{
    
    public GameObject tabuas;
    public GameObject arrow;
    public GameObject CUTSCENE;
    public GameObject DogsCount;
    public GameObject fill;
    public Fogueira fg;

    bool isPressQuest;

    public bool isQuestStart;
    public bool isQuestCompleted;
    public bool fished;
    public bool fogueira;
    public bool killedAll;
    public bool fazerTocha;

    GameManager gm_dogs;

    public StoreUI store;


    //variavel para usar variaveis do script DialogueSystem
    private DialogueSystem dialogueSystem;

    //Nome do NPC
    public string Name;

    //Mexer no tamanho da Area da Caixa de Texto, danos mais espaço para escrever nas frases
    [TextArea(5, 10)]

    public string[] phrases;

    void Start()
    {
        fazerTocha = false;
        fill.SetActive(false);
        isQuestStart = false;
        isQuestCompleted = false;
        CUTSCENE.SetActive(false);
        tabuas.SetActive(false);
        DogsCount.SetActive(false);
        fished = false;
        fogueira = false;
        killedAll = false;
        gm_dogs = GameObject.Find("GameManager").GetComponent<GameManager>();
        dialogueSystem = FindObjectOfType<DialogueSystem>();
        store = GameObject.Find("UI_Shop").GetComponent<StoreUI>();

    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.F) && isPressQuest)
        {
         
            //começou a quest
            isQuestStart = true;
            
            DogsCount.SetActive(true);

        }

        if (store.torch)
        {
            fazerTocha = true;
        }

        if (gm_dogs.DogsKilled >= 3)
        {
            killedAll = true;

        }
        if (fg.estaAceso)
        {

            fogueira = true;
        }

        if (killedAll && fogueira && fished && fazerTocha)
        {
            isQuestCompleted = true;
            dialogueSystem.DropDialogue();
            fill.SetActive(true);

        }

    }


    public void OnTriggerStay(Collider other)
    {
        // ativar o script QuestNivel2
        // assim nao temos muitos scripts a correr ao mesmo tempo
        // e este só fica ativo quando precisamos
        this.gameObject.GetComponent<QuestNivel3>().enabled = true;

        if ((other.gameObject.tag == "Player"))
        {
            FindObjectOfType<DialogueSystem>().EnterRangeOfNPC();
        }

        if ((other.gameObject.tag == "Player") && Input.GetKeyDown(KeyCode.F))
        {
            DogsCount.SetActive(true);
            isPressQuest = true;

            Destroy(arrow);

            this.gameObject.GetComponent<QuestNivel3>().enabled = true;

            dialogueSystem.Names = Name;
            dialogueSystem.dialogueLines = phrases;
            FindObjectOfType<DialogueSystem>().NPCName();
        }
        if ((other.gameObject.tag == "Player" && isQuestCompleted))
        {
            isQuestCompleted = true;
            DogsCount.SetActive(false);
           
            tabuas.SetActive(true);
            CUTSCENE.SetActive(true);
        


        }

    }

    public void OnTriggerExit()
    {
        if (isQuestStart)
        {
          

        }
        if (isQuestCompleted)
        {

          
        }


       
        FindObjectOfType<DialogueSystem>().OutOfRange();
        this.gameObject.GetComponent<QuestNivel3>().enabled = false;
        isPressQuest = false;
       
    }

 





}

