﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tocha : MonoBehaviour
{

    public bool podeAcender = false;
    public GameObject fogo;
    public bool estaAceso = false;

    // Start is called before the first frame update
    void Start()
    {

        fogo.gameObject.SetActive(false);
        gameObject.transform.GetChild(0).gameObject.SetActive(true);

        //parentGameObject.transform.GetChild(0).gameObject.SetActive(false);
        //gameObject.GetComponent<ParticleSystem>().enableEmission = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (podeAcender == false && estaAceso == false)
        {
            fogo.gameObject.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Mouse0) && podeAcender)
        {
            fogo.gameObject.SetActive(true);
            estaAceso = true;
        }

        if (estaAceso)
        {
            fogo.gameObject.SetActive(true);
        }
    }

    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Fogueira")
        {
            Debug.Log("Tocha toca fogueira");
            podeAcender = true;
        }


    }

    void OnTriggerExit(Collider hit)
    {
        if (hit.gameObject.tag == "Fogueira")
        {

            podeAcender = false;
        }
    }
}
