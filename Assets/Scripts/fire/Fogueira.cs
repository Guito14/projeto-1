﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fogueira : MonoBehaviour
{
    public bool podeAcender = false;
    public GameObject fogo;
    public bool estaAceso = false;


    // Start is called before the first frame update
    void Start()
    {

        fogo.gameObject.SetActive(false);
        

    }

    // Update is called once per frame
    void Update()
    {
        if (podeAcender == false && estaAceso == false)
        {
            fogo.gameObject.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Mouse0) && podeAcender)
        {
            fogo.gameObject.SetActive(true);
            estaAceso = true;
        }

        if (estaAceso)
        {
            fogo.gameObject.SetActive(true);
        }
    }

    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Tocha")
        {
            podeAcender = true;
        }


    }

    void OnTriggerExit(Collider hit)
    {
        if (hit.gameObject.tag == "Tocha")
        {

            podeAcender = false;
        }
    }
}
