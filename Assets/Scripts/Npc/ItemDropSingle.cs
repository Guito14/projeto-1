﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDropSingle : MonoBehaviour
{
    // Classe criada para objetos que dropem so 1 tipo de item
    [SerializeField]
    private GameObject item; //Objetos que vão dropar -> ( public GameObject[] )
    private Transform NPCpos; //NPC position
    private int randNum;

    private void Start()
    {

        NPCpos = GetComponent<Transform>();

    }

    public void DropItem()
    {
        //RANDOMIZE
        randNum = Random.Range(0, 101); // 100% total for determining loot chance;
        //INSTACIAR OBJETO
        if (randNum <= 75)
        {
            Instantiate(item, NPCpos.position, Quaternion.identity);
        }
        else if (randNum > 75 && randNum < 95)
        {
            Instantiate(item, NPCpos.position, Quaternion.identity);
            Instantiate(item, NPCpos.position, Quaternion.identity);
        }
        else if (randNum >= 95)
        {
            Instantiate(item, NPCpos.position, Quaternion.identity);
            Instantiate(item, NPCpos.position, Quaternion.identity);
            Instantiate(item, NPCpos.position, Quaternion.identity);
        }
    }
}