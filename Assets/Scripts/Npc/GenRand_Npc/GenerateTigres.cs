﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateTigres : MonoBehaviour
{
    public GameObject tigre;
    public int xPos;
    public int zPos;
    public int xPos1;
    public int zPos1;
    public int tigreCount;

    void Start()
    {
        StartCoroutine(TigreDrop());
    }

    IEnumerator TigreDrop()
    {

        while (tigreCount < 4)
        {
            xPos = Random.Range(-314, -145);
            zPos = Random.Range(340, 1066);
            xPos1 = Random.Range(-605, -315);
            zPos1 = Random.Range(360, 613);
            Instantiate(tigre, new Vector3(xPos, 120, zPos), Quaternion.identity);
            Instantiate(tigre, new Vector3(xPos1, 120, zPos1), Quaternion.identity);
            //yield return new WaitForSeconds(2.0f);
            yield return tigreCount;
            tigreCount++;
        }

        tigreCount = 0;

        while (tigreCount < 3)
        {
            xPos = Random.Range(-1868, -1426);
            zPos = Random.Range(-1510, -36);
            Instantiate(tigre, new Vector3(xPos, 120, zPos), Quaternion.identity);
            //yield return new WaitForSeconds(2.0f);
            yield return tigreCount;
            tigreCount++;
        }
        tigreCount = 0;
        while (tigreCount < 15)
        {
            xPos = Random.Range(-832, -210);
            zPos = Random.Range(-1197, 42);
            Instantiate(tigre, new Vector3(xPos, 120, zPos), Quaternion.identity);
            //yield return new WaitForSeconds(2.0f);
            yield return tigreCount;
            tigreCount++;
        }
    }

}
