﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateDogs : MonoBehaviour
{
    public GameObject dog;
    public int xPos;
    public int zPos;
    public int xPos1;
    public int zPos1;
    public int dogCount;

    void Start()
    {
        StartCoroutine(DogDrop());
    }

    IEnumerator DogDrop() {

        while (dogCount < 25)
        {
            xPos = Random.Range(38, 857);
            zPos = Random.Range(396, 890);
            GameObject instante = Instantiate(dog, new Vector3(xPos, 102, zPos), Quaternion.identity);
            Dog doge = instante.GetComponent<Dog>();
            //yield return new WaitForSeconds(2.0f);
            yield return dogCount;
            dogCount++;
        }

        dogCount = 0;

        while (dogCount < 5)
        {
            xPos = Random.Range(-305, 2);
            zPos = Random.Range(1614, 1928);
            xPos1 = Random.Range(-832, -210);
            zPos1 = Random.Range(-1197, 42);
            Instantiate(dog, new Vector3(xPos1, 102, zPos1), Quaternion.identity);
            Instantiate(dog, new Vector3(xPos, 102, zPos), Quaternion.identity);
            //yield return new WaitForSeconds(2.0f);
            yield return dogCount;
            dogCount++;
        }

        dogCount = 0;

        while (dogCount < 2)
        {
            xPos = Random.Range(-203, -29);
            zPos = Random.Range(1030, 1609);
            Instantiate(dog, new Vector3(xPos, 102, zPos), Quaternion.identity);
            //yield return new WaitForSeconds(2.0f);
            yield return dogCount;
            dogCount++;
        }

        dogCount = 0;
       
    }

}
   

