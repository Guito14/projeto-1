﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateRino : MonoBehaviour
{
    public GameObject rino;
    public int xPos;
    public int zPos;
    public int rinoCount;

    void Start()
    {
        StartCoroutine(RinoDrop());
    }

    IEnumerator RinoDrop()
    {

        while (rinoCount < 15)
        {
            xPos = Random.Range(-1868, -1426);
            zPos = Random.Range(-1510, -36);
            Instantiate(rino, new Vector3(xPos, 100, zPos), Quaternion.identity);
            //yield return new WaitForSeconds(2.0f);
            yield return rinoCount;
            rinoCount++;
        }
    }

}
