﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateCrocs : MonoBehaviour
{
    public GameObject croc;
    public int xPos;
    public int zPos;
    public int crocCount;

    void Start()
    {
        StartCoroutine(CrocDrop());
    }

    IEnumerator CrocDrop()
    {

        while (crocCount < 30)
        {
            xPos = Random.Range(-1868, -922);
            zPos = Random.Range(823, 1051);
            Instantiate(croc, new Vector3(xPos, 100, zPos), Quaternion.identity);
            //yield return new WaitForSeconds(1.0f);
            yield return crocCount;
            crocCount++;
        }
    }
}
