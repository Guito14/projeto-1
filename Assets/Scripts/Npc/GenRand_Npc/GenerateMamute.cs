﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateMamute : MonoBehaviour
{
    public GameObject manute;
    public int xPos;
    public int zPos;
    public int manuteCount;

    void Start()
    {
        StartCoroutine(MamDrop());
    }

    IEnumerator MamDrop()
    {

        while (manuteCount < 15)
        {
            xPos = Random.Range(-832, -210);
            zPos = Random.Range(-1197, 42);
            Instantiate(manute, new Vector3(xPos, 100, zPos), Quaternion.identity);
            //yield return new WaitForSeconds(2.0f);
            yield return manuteCount;
            manuteCount++;
        }
    }



}
