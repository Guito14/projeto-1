﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class QuestNivel1 : MonoBehaviour {

    //seta em cima do npc
    public GameObject arrow;
    //fill amount ui
    public GameObject fill;
    GameManager gm;
    public bool isPressQuest;
    public bool isQuestStart = false;
    public bool isFirstQuestCompleted = false;
 

    //quantidade de macas
    
    public bool isAguaBebidaCompleted = false;



    //variavel para usar variaveis do script DialogueSystem
    private DialogueSystem dialogueSystem;

    //Nome do NPC
    public string Name;

    //Mexer no tamanho da Area da Caixa de Texto, danos mais espaço para escrever nas frases
    [TextArea(5, 10)]

    public string[] phrases;

    void Start () {
        
        dialogueSystem = FindObjectOfType<DialogueSystem>();
        fill.SetActive(false);
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }
	
	void Update () {
       
        if (isAguaBebidaCompleted && gm.macasTotal>= 3)
        {
            isFirstQuestCompleted = true;
          
        }

        if(isFirstQuestCompleted)
        {
            dialogueSystem.DropDialogue();
            fill.SetActive(true);
        }
    

    }

    public void OnTriggerStay(Collider other)
    {
        // ativar o script QuestNivel1
        // assim nao temos muitos scripts a correr ao mesmo tempo
        // e este só fica ativo quando precisamos
        this.gameObject.GetComponent<QuestNivel1>().enabled = true;

            FindObjectOfType<DialogueSystem>().EnterRangeOfNPC();
        

        if ((other.gameObject.tag == "Player") && Input.GetKeyDown(KeyCode.F))
        {
            isQuestStart = true;
            
            Destroy(arrow);
            this.gameObject.GetComponent<QuestNivel1>().enabled = true;
            dialogueSystem.Names = Name;
            dialogueSystem.dialogueLines = phrases;
            FindObjectOfType<DialogueSystem>().NPCName();
        }
        if ((other.gameObject.tag == "Player" && isFirstQuestCompleted))
        {
            isPressQuest = true;
           
           
           
        }

    }

    public void OnTriggerExit()
    {
       
        FindObjectOfType<DialogueSystem>().OutOfRange();
        this.gameObject.GetComponent<QuestNivel1>().enabled = false;
        isPressQuest = false;

    }
}

