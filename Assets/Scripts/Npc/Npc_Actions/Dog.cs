﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dog : MonoBehaviour
{
    /*             
    *                       distance < 50 do Player?                     
    *                           /               \                    
    *                           TRUE             FALSE
    *                              /               \
    *                          Distance < 5      Walking Arround
    *                           
    *                             /   \                 
    *                           Yes     NO            
    *                          /          \
    *                     HIT           Keeps Chasing until Distance > 10
    *                  
    *                  
    *                  
    *
    *
    *
    */

    //    private Transform player;
    //    public float distance;
    //    private float moveSpeed = 3;
    //    public float howClose = 50f;
    //    public float dogDamage = 20f;
    //    public GameObject LookAtEnemy;
    //    int time_lost = 0;
    //    [SerializeField]
    //    private PickUpNew pk;
    //    private VitalsManager vitalsManager;
    //    private EnemyHp enemyHp;
    //    private ArrowBow arrow;

    //    // Start is called before the first frame update
    //    void Start()
    //    {
    //        player = GameObject.FindGameObjectWithTag("Player").transform;
    //        enemyHp = gameObject.GetComponent<EnemyHp>();
    //        vitalsManager = GameObject.Find("FirstPersonPlayer").GetComponent<VitalsManager>();
    //        pk = GameObject.Find("Axe").GetComponent<PickUpNew>();

    //        arrow = GameObject.Find("arrow").GetComponent<ArrowBow>();
    //        LookAtEnemy.SetActive(false);
    //    }

    //    // Update is called once per frame
    //    void Update()
    //    {

    //        distance = Vector3.Distance(player.position, transform.position);
    //        if (distance <= howClose)
    //        {
    //            //transform.LookAt(player);
    //            LookAtEnemy.SetActive(true);
    //            GetComponent<Rigidbody>().AddForce(transform.forward * moveSpeed);

    //            if (distance <= 10)
    //            {

    //                if (time_lost++ == 10) //CUSTOM TIMER
    //                {
    //                    time_lost = 0;
    //                    vitalsManager.TakeDamage(dogDamage);

    //                }

    //                //StartCoroutine(Bite());
    //            }
    //        }

    //        if (distance <= howClose)
    //        {
    //        //    transform.LookAt(player);
    //            GetComponent<Rigidbody>().AddForce(transform.forward * moveSpeed);
    //            LookAtEnemy.SetActive(true);
    //            if (distance <= 10)
    //            {
    //                if (pk.armaApanhada && pk.estaNaMao && pk.item.name == "Axe" && Input.GetKeyDown(KeyCode.Mouse0))
    //                {
    //                    Debug.Log("picked up ");
    //                    enemyHp.hp -= 10.0f;
    //                }

    //                else if (Input.GetKeyDown(KeyCode.Mouse0))
    //                {
    //                    enemyHp.hp -= 1.0f;
    //                }
    //            }
    //        }


    //    }
    //    public void TakeDamage()
    //    {
    //        enemyHp.hp -= 10.0f;
    //    }

    //    void OnCollisionEnter(Collision hit)
    //    {
    //        if (hit.gameObject.tag == "Arrow")
    //        {
    //            enemyHp.hp -= 10.0f;

    //        }

    //    }


    //}


    public enum State
    {
        attack,
        chasing,
        walkArround,

    }

    State currentState;

    VitalsManager player;
    MovementManager npcMovement;
    float distance;
    PickUpNew pk;
    PickUpNew pkk;
    //bool leavestate;

    EnemyHp hp;
    public float howClose = 50.0f;
    private float moveSpeed = 3;


    private ArrowBow arrow;
    // Condições

    DTCondition Distance_50;
    DTCondition Distance_5;


    [SerializeField]
    float damage;



    void Awake()
    {
        arrow = GameObject.Find("arrow").GetComponent<ArrowBow>();
        player = GameObject.Find("FirstPersonPlayer").GetComponent<VitalsManager>();
        hp = GetComponent<EnemyHp>();
        npcMovement = GetComponent<MovementManager>();
        distance = Vector3.Distance(this.transform.position, player.transform.position);
        hp = gameObject.GetComponent<EnemyHp>();
        pk = GameObject.Find("Axe").GetComponent<PickUpNew>();
        pkk = GameObject.Find("Knife").GetComponent<PickUpNew>();

        //DTAction _waits = new DTAction(() => HoldASecond());
        DTAction _hit = new DTAction(() => Ataque());
        DTAction _walk = new DTAction(() => AndarRandom());
        DTAction _chase = new DTAction(() => Perseguir());



        Distance_5 = new DTCondition(() => distance < 5, _hit, _chase);
        Distance_50 = new DTCondition(() => distance < 50, Distance_5, _walk);




    }

    void Update()
    {
        distance = Vector3.Distance(this.transform.position, player.transform.position);
        //npcMovement.info.position.y = 110;
        Distance_50.Run();

        switch (currentState)
        {
            case State.chasing:
                Perseguir();
                break;

            case State.walkArround:
                AndarRandom();
                break;

            default:
                break;
        }

        if (distance <= howClose)
        {
            //    transform.LookAt(player);
            GetComponent<Rigidbody>().AddForce(transform.forward * moveSpeed);
           
            if (distance <= 10)
            {
                if (pk.armaApanhada && pk.estaNaMao && pk.item.name == "Axe" && Input.GetKeyDown(KeyCode.Mouse0))
                {
                    Debug.Log("picked up ");
                    hp.hp -= 10.0f;
                }
                //if (pkk.armaApanhada && pkk.estaNaMao && pkk.item.name == "Knife" && Input.GetKeyDown(KeyCode.Mouse0))
                //{
                //    Debug.Log("picked up");
                //    hp.hp -= 15.0f;
                //}
                else if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    hp.hp -= 1.0f;
                }
            }
        }
    }



    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Arrow")
        {
            hp.hp -= 10.0f;

        }

    }



    public void Ataque()
    {
        player.currentHealth -= damage;

    }
    public void Perseguir()
    {
        foreach (SteeringBehaviour item in npcMovement.lista)
        {
            if (item.name == "Seek")
            {
                npcMovement.steeringBehaviour = item;
            }
        }
    }
    public void AndarRandom()
    {
        foreach (SteeringBehaviour item in npcMovement.lista)
        {
            //nome para random
            if (item.name == "")
            {
                npcMovement.steeringBehaviour = item;
            }
        }
    }



    //IEnumerator HoldASecond()
    //{

    //    yield return new WaitForSeconds(3);
    //    leavestate = false;

    //}

    public void ChangeState(State st)
    {
        if (st != currentState)
        {
            currentState = st;
        }
    }




}




