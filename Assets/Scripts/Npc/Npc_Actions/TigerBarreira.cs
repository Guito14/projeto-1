﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TigerBarreira : MonoBehaviour
{

    private Transform player;
    public float distance;
    private float moveSpeed = 100.0f;
    public float howClose = 200.0f;
    public float tigerDamage = 40.0f;
    private EnemyHp enemyHp;
    private VitalsManager vitalsManager;
    public GameObject LookAtEnemy;
    int time_lost = 0;
    private PickUpNew pk;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        enemyHp = gameObject.GetComponent<EnemyHp>();
        vitalsManager = GameObject.Find("FirstPersonPlayer").GetComponent<VitalsManager>();
        pk = GameObject.Find("Axe").GetComponent<PickUpNew>();
        LookAtEnemy.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

        distance = Vector3.Distance(player.position, transform.position);
        if (distance <= howClose)
        {
            transform.LookAt(player);
            LookAtEnemy.SetActive(true);
            GetComponent<Rigidbody>().AddForce(transform.forward * moveSpeed);

            if (distance <= 10)
            {

                if (time_lost++ == 10) //CUSTOM TIMER
                {
                    time_lost = 0;
                    vitalsManager.TakeDamage(tigerDamage);

                }

                //StartCoroutine(Bite());
            }
        }
        if (distance <= howClose)
        {
            transform.LookAt(player);
            GetComponent<Rigidbody>().AddForce(transform.forward * moveSpeed);
            LookAtEnemy.SetActive(true);
            if (distance <= 10)
            {
                if (pk.armaApanhada && pk.estaNaMao && pk.item.name == "Axe" && Input.GetKeyDown(KeyCode.Mouse0))
                {
                    Debug.Log("picked up ");
                    enemyHp.hp -= 8.0f;
                }
                else if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    enemyHp.hp -= 1.0f;
                }
            }
        }

    }
}
