﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubirArvore : MonoBehaviour
{
    public bool subirArvore;
    public GameObject HudSubirArvore;
    public PlayerMovementScript controller;


    void Start()
    {
        subirArvore = false;
        HudSubirArvore.SetActive(false);
        controller = GameObject.Find("FirstPersonPlayer").GetComponent<PlayerMovementScript>();
        
    }

    void Update()
    {
        if(subirArvore)
        {
            if (Input.GetKeyDown(KeyCode.Y))
            {
                HudSubirArvore.SetActive(false);
                Vector3 position = controller.transform.position;
                position.y += 10f;
                controller.transform.position = position;
                Debug.Log(controller.transform.position.y);

            }
        }
        

    }
    
    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
          HudSubirArvore.SetActive(true);
          subirArvore = true;
           
        }
    }

    void OnTriggerExit(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
          subirArvore = false;
          HudSubirArvore.SetActive(false);
        }
    }

}
