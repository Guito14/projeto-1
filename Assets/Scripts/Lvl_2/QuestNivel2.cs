﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestNivel2 : MonoBehaviour
{
    public GameObject Hud_Quest2Started;
    public GameObject Hud_Quest2Finished;
    public GameObject ponte;
    public GameObject arrow;
    public GameObject CUTSCENE;
    public GameObject fill;
   
    //public GameObject camLevel2;

    bool isPressQuest;

    public  bool isQuestStart;
    public  bool isQuestCompleted;

    public Text txt_nozes;
    private QuestNivel1 quest1;

    //quantidade de nozes
    public int nozesTotal;

    public int cascasTotal;

    //variavel para usar variaveis do script DialogueSystem
    private DialogueSystem dialogueSystem;

    //Nome do NPC
    public string Name;

    //Mexer no tamanho da Area da Caixa de Texto, danos mais espaço para escrever nas frases
    [TextArea(5, 10)]

    public string[] phrases;

    void Start()
    {
        fill.SetActive(false);
        isQuestStart = false;
        isQuestCompleted = false;
        CUTSCENE.SetActive(false);
        ponte.SetActive(false);
        Hud_Quest2Started.SetActive(false);
        Hud_Quest2Finished.SetActive(false);

        quest1 = GameObject.Find("Monge_Lvl_1").GetComponent<QuestNivel1>();
       
        txt_nozes.enabled = false;
        dialogueSystem = FindObjectOfType<DialogueSystem>();
        //camLevel2.SetActive(false);
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.F) && isPressQuest)
        {
            Hud_Quest2Started.SetActive(true);
            //começou a quest
            isQuestStart = true;
            txt_nozes.enabled = true;

        }

        //if (isQuestStart)
        //{           
        //    txt_nozes.text = "" + nozesTotal;
            
        //}

        if (nozesTotal >= 5 && cascasTotal >=5)
        {
            Hud_Quest2Started.SetActive(false);
            isQuestCompleted = true;
            dialogueSystem.DropDialogue();
            fill.SetActive(true);
        }

        //if(isQuestCompleted)
        //{
        //    dialogueSystem.DropDialogue();

        //}


    }
  
    
    public void OnTriggerStay(Collider other)
    {
        // ativar o script QuestNivel2
        // assim nao temos muitos scripts a correr ao mesmo tempo
        // e este só fica ativo quando precisamos
        this.gameObject.GetComponent<QuestNivel2>().enabled = true;

        if ((other.gameObject.tag == "Player"))
        {
            FindObjectOfType<DialogueSystem>().EnterRangeOfNPC();
        }

        if ((other.gameObject.tag == "Player") && Input.GetKeyDown(KeyCode.F))
        {
            
             isPressQuest = true;

            Destroy(arrow);
            
            this.gameObject.GetComponent<QuestNivel2>().enabled = true;
           
            dialogueSystem.Names = Name;
            dialogueSystem.dialogueLines = phrases;
            FindObjectOfType<DialogueSystem>().NPCName();
        }
        if ((other.gameObject.tag == "Player" && isQuestCompleted))
        {
       
            Hud_Quest2Finished.SetActive(true);
            txt_nozes.enabled = false;
            ponte.SetActive(true);
            CUTSCENE.SetActive(true);
            //camLevel2.SetActive(true);


        }

    }

    public void OnTriggerExit()
    {
        if (isQuestStart)
        {
            Hud_Quest2Started.SetActive(true);

        }
        if (isQuestCompleted)
        {

            Hud_Quest2Started.SetActive(false);
        }

        
        Hud_Quest2Started.SetActive(false);
        FindObjectOfType<DialogueSystem>().OutOfRange();
        this.gameObject.GetComponent<QuestNivel2>().enabled = false;
        isPressQuest = false;
        Hud_Quest2Finished.SetActive(false);
    }

    public void NozesTotalCounter()
    {
        if (isQuestStart)
            nozesTotal++;
    }
    public void CascasTotalCounter()
    {
        if (isQuestStart)
            cascasTotal++;
    }


}

