﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthbarScript : MonoBehaviour
{
    private Image HealthBar;
    public float CurrentHealth;
    private float MaxHealth = 100f;
    PlayerMovementScript Player;

    private void Start()
    {
        HealthBar = GetComponent<Image>();
        Player = FindObjectOfType<PlayerMovementScript>();
    }

    private void Update()
    {
        CurrentHealth = Player.Health;
        
        HealthBar.fillAmount = CurrentHealth / MaxHealth;

       
    }
}
