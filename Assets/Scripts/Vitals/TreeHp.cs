﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TreeHp : MonoBehaviour
{
    public float hp;
    public float maxHp;

    private ItemDrop getItem;

    
    void Start()
    {
        maxHp = 100f;
        hp = maxHp;
        getItem = GetComponent<ItemDrop>();
    }

    void Update()
    {
       
        if (hp <= 0)
        {
            if (getItem != null)
            {
                getItem.DropItem();
                Debug.Log("Dropped an Item " + getItem);
            }
            Destroy(gameObject);
        }

        if (hp > maxHp)
            hp = maxHp;
    }
}
