﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VitalsManager : MonoBehaviour
{
    public float maxHealth = 100f, maxHunger = 100f, maxThirsth = 100f;
    public float currentHealth, currentHunger, currentThirsth;
    private float hunger = 5f, thirsth = 5f;
    private int time_lose, time_lost = 0;

    public HealthBar healthBar;
    public Hungerbar hungerBar;
    public Thirsthbar thirsthBar;

    private bool invoked = true;
    private bool invoked_complete = false;

    
     DeadMenu deadMenu;


    // Start is called before the first frame update
    void Start()
    {

        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);

        currentHunger = maxHunger;
        hungerBar.SetMaxHunger(maxHunger);

        currentThirsth = maxThirsth;
        thirsthBar.SetMaxThirsth(maxThirsth);

        StartCoroutine(LoseHungerTime());
        StartCoroutine(LoseThirsthTime());
     
        deadMenu = GameObject.Find("DeadCanvas").GetComponent<DeadMenu>();
    }

    void Update()
    {
        if (currentHunger <= 0)
        {

            if (time_lost++ == 150) //CUSTOM TIMER
            {
                time_lost = 0;
                invoked_complete = true;
            }
            if (invoked_complete)
            {
                TakeDamage(10f);
            }
        }
        if (currentThirsth <= 0)
        {
            if (time_lose++ == 150) //CUSTOM TIMER
            {
                time_lose = 0;
                invoked = false;
            }
            if (!invoked)
            {
                TakeDamage(10f);
            }

        }

        if (currentHunger == maxHunger && currentThirsth == maxThirsth)
        {
            EarnHealth(100f);

        }
        if (currentHealth <= 0)
        {
            //SceneManager.LoadScene("Menu");
            //deadMenu.Dead();
            deadMenu.Dead();
        }
    }



    public void TakeDamage(float damage)
    {
        currentHealth -= damage;
        healthBar.SetHealth(currentHealth);
    }



    IEnumerator LoseHungerTime()     // hungerBar --
    {
        while (hunger >= 0)
        {
            currentHunger -= hunger;
            hungerBar.SetHunger(currentHunger);
            yield return new WaitForSeconds(15);
        }

    }

    IEnumerator LoseThirsthTime()     // thirsthBar --
    {
        while (thirsth >= 0)
        {
            currentThirsth -= thirsth;
            thirsthBar.SetThirsth(currentThirsth);
            yield return new WaitForSeconds(10);
        }

    }
    public void EarnHunger(float hunger)
    {
        currentHunger += hunger;
        hungerBar.SetHunger(currentHunger);
        if (currentHunger == maxHunger)
            hungerBar.SetHunger(maxHunger);
    }
    public void EarnThirsth(float thirsth)
    {
        currentThirsth = currentThirsth + thirsth;
        thirsthBar.SetThirsth(currentThirsth);
        if (currentThirsth >= maxThirsth)
        {
            currentThirsth = maxThirsth;
            thirsthBar.SetThirsth(maxThirsth);
        }
    }
    public void EarnHealth(float health)
    {
        currentHealth = currentHealth + health;
        healthBar.SetHealth(currentHealth);
        if (currentHealth >= maxHealth)
        {
            currentHealth = maxHealth;
            healthBar.SetHealth(maxHealth);
        }
    }
    public void LoseThirst(float thirsth)
    {
        currentThirsth = currentThirsth - thirsth;
        thirsthBar.SetThirsth(currentThirsth);
    }

    public float Hunger()
    {
        return currentHunger;
    }

}
