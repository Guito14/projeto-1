﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fishing : MonoBehaviour
{
    //bool isBeberAgua;
    bool canFish = false;
    //public GameObject HudPesca;
    private VitalsManager vitalsManager;
    private PlayerMovementScript playermoviment;
    // private questMaca questmaca;
    private QuestNivel3 questNivel3;
    private int time_lose = 0;
    private bool looking = false;
    private bool pressed = false;
    private int randNum; // chooses a random number to see if loot os dropped- Loot chance
    [SerializeField]
    InventoryNew inv;
    [SerializeField]
    PickUpNew item;
    GameManager gm;
    //public Thirsthbar thristbar;


    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        //HudPesca.SetActive(false);
        playermoviment = GameObject.Find("FirstPersonPlayer").GetComponent<PlayerMovementScript>();
        vitalsManager = GameObject.Find("FirstPersonPlayer").GetComponent<VitalsManager>();
        // questmaca = GameObject.Find("Monge_Lvl_1").GetComponent<questMaca>();
        questNivel3 = GameObject.Find("MongeNivel3").GetComponent<QuestNivel3>();
    }

    void Update()
    {
        if (questNivel3.isQuestStart)
        {
            if (canFish)
            {
                //HudBeberAgua.SetActive(true);

                if (Input.GetKey(KeyCode.Mouse1)) //KEY PRESSED
                {
                    playermoviment.fishing = true;   //SET DRINKING TO CROUCH 
                    //playermoviment.transform.rotation = Quaternion.Euler(50, playermoviment.transform.rotation.y, playermoviment.transform.rotation.z); //SET ROTATION OF PLAYER

                    looking = true;
                    pressed = true;


                    if (time_lose++ == 600) //CUSTOM TIMER
                    {
                        time_lose = 0;
                        Pescar();
                    }
                    if (vitalsManager.currentHunger == vitalsManager.maxHunger)
                    {
                        playermoviment.fishing = false;
                        looking = false;
                        if (pressed)
                        {
                            playermoviment.transform.rotation = Quaternion.Euler(0, playermoviment.transform.rotation.y, playermoviment.transform.rotation.z);
                        }
                        pressed = false;
                        //playermoviment.transform.rotation = Quaternion.Euler(playermoviment.transform.rotation.x, playermoviment.transform.rotation.y, playermoviment.transform.rotation.z); //SET ROTATION OF PLAYER; //SET ROTATION OF PLAYER DEFAULT
                    }
                    questNivel3.fished = true; // Ja pescou! 
                                               // adicionar o som de beber

                }
                else
                {
                    playermoviment.fishing = false;
                    looking = false;
                    if (pressed)
                    {
                        playermoviment.transform.rotation = Quaternion.Euler(0, playermoviment.transform.rotation.y, playermoviment.transform.rotation.z);
                    }
                    pressed = false;

                    //playermoviment.transform.rotation = Quaternion.Euler(playermoviment.transform.rotation.x, playermoviment.transform.rotation.y, playermoviment.transform.rotation.z); //SET ROTATION OF PLAYER; //SET ROTATION OF PLAYER DEFAULT

                }
            }


        }

        if (looking)
        {
            playermoviment.transform.LookAt(transform);
        }
    }

    private void Pescar()
    {

        randNum = Random.Range(0, 51);
        Debug.Log("Random Number is " + randNum);


        if (randNum <= 10)
        {
            /*  HudPesca.SetActive(true); */ //HUD A DIZER PESCOU !!
                                             //vitalsManager.EarnHunger(10); //Add Hunger
            Debug.Log("picking up" + item.name);
            inv.AddItem(Instantiate(item, this.transform.position, Quaternion.identity));// adicionar ao inventario 
            gm.fishCounter();

        }
        else if (randNum > 10)
        {
            Pescar();
        }

    }


    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {

            canFish = true;

        }
    }

    void OnTriggerExit(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            pressed = false;
            looking = false;
            canFish = false;
            //HudPesca.SetActive(false);
        }
    }


}
