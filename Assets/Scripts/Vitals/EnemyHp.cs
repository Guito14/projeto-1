﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHp : MonoBehaviour
{
    public float hp = 100;
    public float maxHp;

    public GameObject hpUI;
    public Slider slider;

    private ItemDrop getItem;
    GameManager gm;

    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        hp = maxHp;
        slider.value = CalculateHP();
        getItem = GetComponent<ItemDrop>();
    }

    // Update is called once per frame
    void Update()
    {
        slider.value = CalculateHP();

        if (hp < maxHp)
            hpUI.SetActive(true);

        if (hp <= 0)
        {
            if (getItem != null)
            {
                getItem.DropItem();
                Debug.Log("Dropped an Item " + getItem);
            }
            Destroy(gameObject);

            if (gameObject.tag == "Dog")
            {
                gm.DogsKilledCounter();
            }
            if (gameObject.tag == "Tiger")
            {
                gm.tigersKilledCount();
            }

        }

        if (hp > maxHp)
            hp = maxHp;
    }

    float CalculateHP()
    {
        return hp / maxHp;
    }
}
