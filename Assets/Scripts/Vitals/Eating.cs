﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Eating : MonoBehaviour
{
    bool apanharComida;
    public ParticleSystem particleLauncher;
    public GameObject DestinationFood;
    public Transform interationZone;
    public GameObject food;


    public VitalsManager vitalsManager;

    public InventoryNew inv;
    public PickUpNew item;

    public void Start()
    {
        //vitalsManager = GetComponent<VitalsManager>();
        vitalsManager = GameObject.Find("FirstPersonPlayer").GetComponent<VitalsManager>();
        //inv = GameObject.Find("Inventory").GetComponent<InventoryNew>();
        //  item = GameObject.Find("Carne").GetComponent<PickUpNew>();


    }
    public void Update()
    {

        if (Input.GetKeyDown(KeyCode.T) && apanharComida)
        {

            this.transform.SetParent(interationZone);
            this.transform.position = DestinationFood.transform.position;

            // this.transform.position = naMao.position;
            // this.transform.parent = GameObject.Find("Destination").transform;
            this.GetComponent<Rigidbody>().useGravity = false;
            this.GetComponent<Rigidbody>().isKinematic = true;
            ParticleLaunch();// invoca a função depois de 1segundo apos o E ser pressionado
            //Invoke("Eat", 4);
            Eat();
            inv.RemoveItem(item.item);

            Destroy(this.gameObject);


        }

    }


    public void ParticleLaunch()
    {
        particleLauncher.Emit(10); //emite salpicos
        /*Invoke("Eat", 1);*/ // invoca a função depois de 1segundo apos o a ParticleLaunch ser invocada
    }

    public void Eat()
    {
        vitalsManager.currentHunger += 5;
        vitalsManager.hungerBar.SetHunger(vitalsManager.currentHunger);
        if (vitalsManager.currentHunger == vitalsManager.maxHunger)
            vitalsManager.hungerBar.SetHunger(vitalsManager.maxHunger);

        //adicionar audio a dar uma trinca
    }
    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {

            apanharComida = true;

        }
    }

    void OnTriggerExit(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            apanharComida = false;
        }
    }

}