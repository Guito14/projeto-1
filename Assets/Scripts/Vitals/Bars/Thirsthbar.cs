﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Thirsthbar : MonoBehaviour
{

    public Slider slider;

    public void SetMaxThirsth(float thirsth)
    {
        slider.maxValue = thirsth;
        slider.value = thirsth;
    }

    public void SetThirsth(float thirsth)
    {
        slider.value = thirsth;
    }
}
