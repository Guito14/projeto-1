﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drinking : MonoBehaviour
{
    bool isBeberAgua;
    public GameObject HudBeberAgua;
    private VitalsManager vitalsManager;
    private PlayerMovementScript playermoviment;
    //private questMaca questmaca;
    private QuestNivel1 questNivel1;
    private int time_lose = 0;
    private bool looking = false;
    private bool pressed = false;

    //public Thirsthbar thristbar;


    void Start()
    {
        HudBeberAgua.SetActive(false);
        playermoviment = GameObject.Find("FirstPersonPlayer").GetComponent<PlayerMovementScript>();
        vitalsManager = GameObject.Find("FirstPersonPlayer").GetComponent<VitalsManager>();
        questNivel1 = GameObject.Find("Monge_Lvl_1").GetComponent<QuestNivel1>();
    }

    void Update()
    {


        if (vitalsManager.currentThirsth == vitalsManager.maxThirsth) //VERIFY IF WATER IS FULL
        {
            playermoviment.drinking = false;
            HudBeberAgua.SetActive(false);
        }
        else
        {
            if (isBeberAgua)
            {
                HudBeberAgua.SetActive(true);
                if (Input.GetKey(KeyCode.F)) //KEY PRESSED
                {
                    playermoviment.drinking = true; //SET DRINKING TO CROUCH 
                    //playermoviment.transform.rotation = Quaternion.Euler(50, playermoviment.transform.rotation.y, playermoviment.transform.rotation.z); //SET ROTATION OF PLAYER

                    looking = true;
                    pressed = true;

                    if (time_lose++ == 30) //CUSTOM TIMER
                    {
                        time_lose = 0;
                        beber();
                    }
                    if (vitalsManager.currentThirsth == vitalsManager.maxThirsth)
                    {
                        playermoviment.drinking = false;
                        looking = false;
                        if (pressed)
                        {
                            playermoviment.transform.rotation = Quaternion.Euler(0, playermoviment.transform.rotation.y, playermoviment.transform.rotation.z);
                        }
                        pressed = false;
                        //playermoviment.transform.rotation = Quaternion.Euler(playermoviment.transform.rotation.x, playermoviment.transform.rotation.y, playermoviment.transform.rotation.z); //SET ROTATION OF PLAYER; //SET ROTATION OF PLAYER DEFAULT
                    }
                    questNivel1.isAguaBebidaCompleted = true;
                    // adicionar o som de beber
                }
                else
                {
                    playermoviment.drinking = false;
                    looking = false;
                    if (pressed)
                    {
                        playermoviment.transform.rotation = Quaternion.Euler(0, playermoviment.transform.rotation.y, playermoviment.transform.rotation.z);
                    }
                    pressed = false;

                    //playermoviment.transform.rotation = Quaternion.Euler(playermoviment.transform.rotation.x, playermoviment.transform.rotation.y, playermoviment.transform.rotation.z); //SET ROTATION OF PLAYER; //SET ROTATION OF PLAYER DEFAULT

                }
            }

        }

        if (looking)
        {
            playermoviment.transform.LookAt(transform);
        }
    }

    private void beber()
    {
        //ADD THIRSTH
        vitalsManager.EarnThirsth(5);
    }


    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {

            isBeberAgua = true;

        }
    }

    void OnTriggerExit(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            pressed = false;
            looking = false;
            isBeberAgua = false;
            HudBeberAgua.SetActive(false);
        }
    }


}
