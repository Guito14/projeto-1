﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberofTigersKilled : MonoBehaviour
{
    public GameObject tigerIcon;
    void Start()
    {
        tigerIcon.SetActive(false);
    }
    void Update()
    {
        GetComponent<Text>().text = "" + GameManager.instanciar.TigersKilled;

        if (GameManager.instanciar.TigersKilled < 1)
        {
            GetComponent<Text>().color = Color.red;
        }
        else if (GameManager.instanciar.TigersKilled < 3)
        {
            GetComponent<Text>().color = Color.yellow;
        }
        else if (GameManager.instanciar.DogsKilled >= 5)
        {
            GetComponent<Text>().color = Color.green;
        }
        tigerIcon.SetActive(true);
    }
}
