﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierLvl : MonoBehaviour
{


    public GameObject barrelAnimal1;
    public GameObject barrelAnimal2;
    public GameObject barrelAnimal3;
    public GameObject barrelAnimal4;
    public GameObject barrelAnimal5;

    // private questMaca questmaca;
    private QuestNivel1 questNivel1;


    //public GameObject HudBarrierMessage;

    void Start()
    {
        questNivel1 = GameObject.Find("Monge_Lvl_1").GetComponent<QuestNivel1>();

        //HudBarrierMessage.SetActive(false);
    }

    void Update()
    {
        if (questNivel1.isQuestStart)
        {
            
            //verificar se a quest esta completa
            if (questNivel1.isFirstQuestCompleted)
            {
                //levantar a barreira
                gameObject.SetActive(false);
                Destroy(barrelAnimal1);
                Destroy(barrelAnimal2);
                Destroy(barrelAnimal3);
                Destroy(barrelAnimal4);
                Destroy(barrelAnimal5);
                
            }

        }
         
    }

//    void OnTriggerEnter(Collider hit)
//    {
//        if (hit.gameObject.tag == "Player")
//        {
//            HudBarrierMessage.SetActive(true);
//        }
        
//    }

//    void OnTriggerExit(Collider hit)
//    {
//        if (hit.gameObject.tag == "Player")
//        {
//            HudBarrierMessage.SetActive(false);
//        }
//    }
}
