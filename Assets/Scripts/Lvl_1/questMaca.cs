﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class questMaca : MonoBehaviour
{
    public GameObject HUD_NPCQuestMaca;
    public GameObject Hud_NPCQuestMacaCompleted;
    public GameObject arrow;

    bool isPressQuest;
    public bool isQuestStart = false;
    public bool isFirstQuestCompleted = false;
    public Text txt_macas;
    //quantidade de macas
    public int macasTotal;
    public bool isAguaBebidaCompleted = false;



    void Start()
    {
      
        HUD_NPCQuestMaca.SetActive(false);
        Hud_NPCQuestMacaCompleted.SetActive(false);

        
        txt_macas.enabled = false;
         
    }

    void Update()
    {
  
        if (Input.GetKeyDown(KeyCode.E) && isPressQuest)
        {
            HUD_NPCQuestMaca.SetActive(true);
            //começou a quest
            isQuestStart = true;
            txt_macas.enabled = true;

            
            Destroy(arrow);
        }

        if (isQuestStart)
        {

            txt_macas.text = "" + macasTotal;
        }

        if (Input.GetKeyDown(KeyCode.E) && macasTotal >= 3 && isAguaBebidaCompleted)
        {
            HUD_NPCQuestMaca.SetActive(false);
            isFirstQuestCompleted = true;
        }

        

    }

    void OnTriggerEnter(Collider hit)
    {
        if(hit.gameObject.tag == "Player" )
        {
            isPressQuest = true;
        }

        if (hit.gameObject.tag == "Player" && isFirstQuestCompleted)
        { 
            isPressQuest = true;
            Hud_NPCQuestMacaCompleted.SetActive(true);
            txt_macas.enabled = false;
        }
    }
    
    void OnTriggerExit(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            HUD_NPCQuestMaca.SetActive(false);
            isPressQuest = false;
            Hud_NPCQuestMacaCompleted.SetActive(false);
        }
    }


}
