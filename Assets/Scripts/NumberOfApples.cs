﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberOfApples : MonoBehaviour
{
    public GameObject appleIcon;

    void Start()
    {
        appleIcon.SetActive(false);
    }

    void Update()
    {
        GetComponent<Text>().text = "" + GameManager.instanciar.macasTotal;

        if (GameManager.instanciar.macasTotal < 1)
        {
            GetComponent<Text>().color = Color.red;
        }
        else if (GameManager.instanciar.macasTotal == 2)
        {
            GetComponent<Text>().color = Color.yellow;
        }
        else if (GameManager.instanciar.macasTotal >= 3)
        {
            GetComponent<Text>().color = Color.green;
        }
        appleIcon.SetActive(true);
    }
}
