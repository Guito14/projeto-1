﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour
{

    private Transform player;
    public float distance;
    public float howClose = 30f;
    private TreeHp treeHp;


    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        treeHp = gameObject.GetComponent<TreeHp>();
    }

    // Update is called once per frame
    void Update()
    {

        distance = Vector3.Distance(player.position, transform.position);
        if (distance <= howClose)
        {
          
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                if (distance <= 10)
                {

                    treeHp.hp -= 10;
                }

            }
        }




    }
}
