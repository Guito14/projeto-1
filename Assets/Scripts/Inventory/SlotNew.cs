﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlotNew : MonoBehaviour
{
    public Image icon = null;
    public Text txt_itemTotal;
    public Item item;
    public int itemtotal = 0;
    //public Text txt_itemName;
    //public string itemName;

    void Start()
    {
        icon.sprite = null;
        icon.enabled = false;
    }
    void Update()
    {
        txt_itemTotal.text = "" + itemtotal;
        //txt_itemName.text = "" + itemName;
    }

    public void AddItem(Item NewItem)
    {
        if (item == null)
        {
            item = NewItem;
            icon.sprite = item.icon;
            icon.enabled = true;
            //itemName = item.name;

        }
        itemtotal++;

    }

    public void RemoveSlot()
    {
        if (itemtotal > 1)
        {
            itemtotal--;
        }
        else
        {
            item = null;
            icon.sprite = null;
            icon.enabled = false;
            //itemName = "";
            itemtotal = 0;

        }

    }

}
