﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryNew : MonoBehaviour
{
    public List<PickUpNew> itemsList;
    [SerializeField]
    List<SlotNew> slotList;
    [SerializeField]
    GameObject goToHand;
    [SerializeField]
    GameObject inventoryUI;
    bool uiActive;

    public InventoryNew()
    {

    }

    void Update()
    {
        if (Input.GetButtonDown("Inventory"))
        {
            if (uiActive)
            {

                uiActive = false;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = false;
            }
            else
            {

                uiActive = true;

                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }


    }

    public void AddItem(PickUpNew item)
    {
        itemsList.Add(item);
        for (int i = 0; i < itemsList.Count; i++)
        {
            if (slotList[i].item == item.item || slotList[i].item == null)
            {
                slotList[i].AddItem(item.item);
                break;
            }

        }


    }


    public void RemoveItem(Item item)
    {

        for (int i = 0; i < slotList.Count; i++)
        {
            //ver na slotList onde é que está o item que queremos remover
            // quando chegar ao item, remover da lista e fazemos o RemoveSlot
            // Remover o que nos quisermos
            if (slotList[i].item == item)
            {
                removeItem_pickUP(item);
                slotList[i].RemoveSlot();
                break;
            }
        }

    }
    public void removeItem_pickUP(Item item)
    {
        for (int i = 0; i < itemsList.Count; i++)
        {
            if (itemsList[i].item == item)
            {
                itemsList.RemoveAt(i);
                break;
            }
        }
    }
    public int howMany(Item item)
    {
        int count = 0;
        for (int i = 0; i < itemsList.Count; i++)
        {
            if (item == itemsList[i].item)
            {

                count++;

            }

        }
        Debug.Log(count + item.name);
        return count;
    }


    public void RemoveX_Items(int num, Item item)
    {
        if (num <= howMany(item))
        {
            for (int i = num; i > 0; i--)
            {
                RemoveItem(item);

            }
        }
        else
            Debug.Log(" Número Inválido" + num);
    }


    //public void UpdateSlot()
    //{

    //}

    public void PickItem(int slot)
    {

        foreach (PickUpNew item in itemsList)
        {

            if (slotList[slot].item == item.item)
            {
                item.itemEspecifico.transform.SetParent(goToHand.transform);
                item.Equip();
                break;
            }


        }
        foreach (PickUpNew item in itemsList)
        {
            if (slotList[slot].item != item.item)
            {
                item.itemEspecifico.SetActive(false);
            }
        }

    }


}
