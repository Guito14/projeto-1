﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpNew : MonoBehaviour
{
    public Item item;
    public GameObject itemEspecifico;
    public GameObject text_apanharArma;
    [SerializeField]
    InventoryNew inv;

    public bool apanharArma;
    public bool armaApanhada;
    public bool estaNaMao;


    public Transform interationZone;
    public GameObject Destination;
    public Transform BackPack;
    ArmsAnimation aNim;


    Rocha r;
    Bow bow;
    GameManager gm;
    QuestNivel2 q2;

    void Start()
    {

        aNim = GameObject.Find("ArmsFinal").GetComponent<ArmsAnimation>();
        bow = GameObject.Find("Bow").GetComponent<Bow>();
        r = GameObject.Find("RockSmash").GetComponent<Rocha>();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        q2 = GameObject.Find("Monge_Lvl_2").GetComponent<QuestNivel2>();
        text_apanharArma.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && apanharArma && !armaApanhada)
        {
            armaApanhada = true;
            apanharArma = false;
            inv.AddItem(this);
            gameObject.SetActive(false);
            this.transform.SetParent(BackPack);
            estaNaMao = false;
            text_apanharArma.SetActive(false);

            if (item.name == "Maca")
            {
                gm.MacasTotalCounter();
            }
            if (item.name == "Noz")
            {
                q2.NozesTotalCounter();
            }



        }

        if (estaNaMao)
        {
            text_apanharArma.SetActive(false);
        }



        if (Input.GetKeyDown(KeyCode.G) && estaNaMao)
        {
            this.transform.SetParent(null);
            this.GetComponent<Rigidbody>().useGravity = true;
            this.GetComponent<Rigidbody>().isKinematic = false;
            this.apanharArma = false;
            this.armaApanhada = false;
            inv.RemoveItem(this.item);
            estaNaMao = false;
            bow.canShoot = false;
            r.canSmash = false;

        }

        if (armaApanhada && Input.GetKeyDown(KeyCode.Mouse0))
        {

            if (item.name == "Axe")
            {
                //r.canSmash = false;
                GetComponent<Animator>().Play("axeAnimation_02");
            }

            if (item.name == "Pedra")
            {
                r.canSmash = true;
            }
            else
            {
                r.canSmash = false;
            }
        }

        //if (armaApanhada && Input.GetKeyDown(KeyCode.Mouse0))
        //{
        //    if (item.name == "Pedra")
        //    {
        //        r.canSmash = true;
        //    }
        //    else
        //    {
        //        r.canSmash = false;
        //    }
        //}
    }

    public void Equip()
    {
        r.canSmash = false;
        estaNaMao = true;
        apanharArma = false;
        this.transform.SetParent(interationZone);
        this.transform.position = Destination.transform.position;
        this.transform.rotation = Destination.transform.rotation;
        this.GetComponent<Rigidbody>().useGravity = false;
        this.GetComponent<Rigidbody>().isKinematic = true;
        gameObject.SetActive(true);



    }


    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            apanharArma = true;
            text_apanharArma.SetActive(true);

        }
    }

    void OnTriggerExit(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            apanharArma = false;
            text_apanharArma.SetActive(false);
            armaApanhada = false;

        }
    }
}
