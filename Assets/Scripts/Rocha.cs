﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocha : MonoBehaviour
{
    public bool canSmash;
    public bool podePartir = false;
    

    // Start is called before the first frame update
    void Start()
    {
        //canSmash = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.H))
        {
            GetComponent<Animator>().Play("Rock");
        }
        

    }


    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Noz")
        {
            podePartir = true;
        }


    }

    void OnTriggerExit(Collider hit)
    {
        if (hit.gameObject.tag == "Noz")
        {
            podePartir = false;
           
        }
    }

}
