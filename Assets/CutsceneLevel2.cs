﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneLevel2 : MonoBehaviour
{
    public GameObject thePlayer;
    public GameObject cutSceneCam;

    QuestNivel2 quest2;

    void Start()
    {
        this.gameObject.GetComponent<CutsceneLevel2>().enabled = false;
        quest2 = GameObject.Find("Monge_Lvl_2").GetComponent<QuestNivel2>();
    }

    void OnTriggerEnter(Collider other)
    {
        this.gameObject.GetComponent<CutsceneLevel2>().enabled = true;
        if (other.gameObject.tag == "Player" && quest2.isQuestCompleted)
        {      
            cutSceneCam.SetActive(true);
            thePlayer.SetActive(false);
            StartCoroutine(FinishCut());

        }
     
    }


    IEnumerator FinishCut()
    {
        yield return new WaitForSeconds(10);
        thePlayer.SetActive(true);
        cutSceneCam.SetActive(false);
    }
}
